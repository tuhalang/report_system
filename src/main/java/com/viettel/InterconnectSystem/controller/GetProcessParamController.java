package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.GetProcessParam;
import com.viettel.InterconnectSystem.service.GetProcessParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class GetProcessParamController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetProcessParamController.class);

    @Autowired
    private GetProcessParamService getProcessParamService;


    @RequestMapping(value = "/get-process-params", method = RequestMethod.GET)
    public ResponseEntity getProcessParam(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    getProcessParamService.findAll(pageable, responseBean, deleted);
                }else{
                    getProcessParamService.searchByIsDeleteAndProcessCode(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/get-process-params", method = RequestMethod.PUT)
    public ResponseEntity updateProcessParam(@RequestBody GetProcessParam getProcessParam){
        ResponseBean responseBean = new ResponseBean();
        try {
            getProcessParamService.update(getProcessParam, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/get-process-params", method = RequestMethod.DELETE)
    public ResponseEntity deleteProcessParam(@RequestParam Long getId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            getProcessParamService.delete(getId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/get-process-params", method = RequestMethod.POST)
    public ResponseEntity createProcessParam(@RequestBody GetProcessParam getProcessParam){
        ResponseBean responseBean = new ResponseBean();
        try {
            getProcessParamService.create(getProcessParam, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
