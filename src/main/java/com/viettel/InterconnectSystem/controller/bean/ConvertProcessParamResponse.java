package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.ConvertProcessParam;

public class ConvertProcessParamResponse {

    private ConvertProcessParam convertProcessParam;
    private String switchName;

    public ConvertProcessParamResponse(ConvertProcessParam convertProcessParam, String switchName) {
        this.convertProcessParam = convertProcessParam;
        this.switchName = switchName;
    }

    public ConvertProcessParam getConvertProcessParam() {
        return convertProcessParam;
    }

    public void setConvertProcessParam(ConvertProcessParam convertProcessParam) {
        this.convertProcessParam = convertProcessParam;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }
}
