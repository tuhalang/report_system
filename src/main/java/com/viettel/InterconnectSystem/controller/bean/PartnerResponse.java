package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.Partner;

public class PartnerResponse extends Partner{

    private String countryName;

    public PartnerResponse(Long partnerId, Long countryId, String address, String phone, String fax, String partName, String partAbbreviate, Integer isDelete, Integer isNational, Integer isVas, Integer timezoneOutDiff, Integer timezoneInDiff, String countryName) {
        super(partnerId, countryId, address, phone, fax, partName, partAbbreviate, isDelete, isNational, isVas, timezoneOutDiff, timezoneInDiff);
        this.countryName = countryName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
