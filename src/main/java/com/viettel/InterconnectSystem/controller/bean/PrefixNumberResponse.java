package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.PrefixNumber;

public class PrefixNumberResponse {

    private PrefixNumber prefixNumber;
    private String countryName;
    private String serviceName;
    private String areaName;
    private String partName;

    public PrefixNumberResponse(PrefixNumber prefixNumber, String countryName, String serviceName, String areaName, String partName) {
        this.prefixNumber = prefixNumber;
        this.countryName = countryName;
        this.serviceName = serviceName;
        this.areaName = areaName;
        this.partName = partName;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public PrefixNumber getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(PrefixNumber prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
