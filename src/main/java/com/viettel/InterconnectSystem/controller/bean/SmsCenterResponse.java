package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.SmsCenter;

public class SmsCenterResponse {

    private SmsCenter smsCenter;
    private String partName;

    public SmsCenterResponse(SmsCenter smsCenter, String partName) {
        this.smsCenter = smsCenter;
        this.partName = partName;
    }

    public SmsCenter getSmsCenter() {
        return smsCenter;
    }

    public void setSmsCenter(SmsCenter smsCenter) {
        this.smsCenter = smsCenter;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }
}
