package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.SwitchBoard;

public class SwitchBoardResponse {

    private SwitchBoard switchBoard;
    private String areaName;

    public SwitchBoardResponse(SwitchBoard switchBoard, String areaName) {
        this.switchBoard = switchBoard;
        this.areaName = areaName;
    }

    public SwitchBoard getSwitchBoard() {
        return switchBoard;
    }

    public void setSwitchBoard(SwitchBoard switchBoard) {
        this.switchBoard = switchBoard;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
