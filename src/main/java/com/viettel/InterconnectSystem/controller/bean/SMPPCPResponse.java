package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.SMPPCP;

public class SMPPCPResponse {

    private SMPPCP smppCp;
    private String partName;
    private String switchName;

    public SMPPCPResponse(SMPPCP smppCp, String partName, String switchName) {
        this.smppCp = smppCp;
        this.partName = partName;
        this.switchName = switchName;
    }

    public SMPPCP getSmppCp() {
        return smppCp;
    }

    public void setSmppCp(SMPPCP smppCp) {
        this.smppCp = smppCp;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }
}
