package com.viettel.InterconnectSystem.controller.bean;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

public class FileUpload implements Serializable {

    private String interType;
    private Long partnerId;
    private String partType;
    private Date dateCreate;
    private Integer isDelete;
    private String direction;
    private String codePriceName;
    private String connectType;
    private String priceTableType;
    private File file;
    private Integer destination;
    private Integer areaCode;
    private Integer rate;
    private Integer rateValid;
    private Integer rowStart;
    private Integer rowEnd;
    private Long currencyId;

    public FileUpload(String interType, Long partnerId, String partType, Date dateCreate, String direction, String codePriceName, String connectType, File file, Integer destination, Integer areaCode, Integer rate, Integer rateValid, Integer rowStart, Integer rowEnd, Long currencyId) {
        this.interType = interType;
        this.partnerId = partnerId;
        this.partType = partType;
        this.dateCreate = dateCreate;
        this.direction = direction;
        this.codePriceName = codePriceName;
        this.connectType = connectType;
        this.file = file;
        this.destination = destination;
        this.areaCode = areaCode;
        this.rate = rate;
        this.rateValid = rateValid;
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
        this.currencyId = currencyId;
    }

    public FileUpload(String interType, Long partnerId, String partType, Date dateCreate, Integer isDelete, String direction, String codePriceName, String connectType, String priceTableType, File file, Integer destination, Integer areaCode, Integer rate, Integer rateValid, Integer rowStart, Integer rowEnd, Long currencyId) {
        this.interType = interType;
        this.partnerId = partnerId;
        this.partType = partType;
        this.dateCreate = dateCreate;
        this.isDelete = isDelete;
        this.direction = direction;
        this.codePriceName = codePriceName;
        this.connectType = connectType;
        this.priceTableType = priceTableType;
        this.file = file;
        this.destination = destination;
        this.areaCode = areaCode;
        this.rate = rate;
        this.rateValid = rateValid;
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
        this.currencyId = currencyId;
    }

    public FileUpload(String interType, Long partnerId, String partType, Date dateCreate, Integer isDelete, String direction, String codePriceName, String connectType, String priceTableType, File file, Integer destination, Integer areaCode, Integer rate, Integer rateValid, Integer rowStart, Integer rowEnd) {
        this.interType = interType;
        this.partnerId = partnerId;
        this.partType = partType;
        this.dateCreate = dateCreate;
        this.isDelete = isDelete;
        this.direction = direction;
        this.codePriceName = codePriceName;
        this.connectType = connectType;
        this.priceTableType = priceTableType;
        this.file = file;
        this.destination = destination;
        this.areaCode = areaCode;
        this.rate = rate;
        this.rateValid = rateValid;
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public Integer getDestination() {
        return destination;
    }

    public void setDestination(Integer destination) {
        this.destination = destination;
    }

    public Integer getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(Integer areaCode) {
        this.areaCode = areaCode;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Integer getRateValid() {
        return rateValid;
    }

    public void setRateValid(Integer rateValid) {
        this.rateValid = rateValid;
    }

    public String getInterType() {
        return interType;
    }

    public void setInterType(String interType) {
        this.interType = interType;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCodePriceName() {
        return codePriceName;
    }

    public void setCodePriceName(String codePriceName) {
        this.codePriceName = codePriceName;
    }

    public String getConnectType() {
        return connectType;
    }

    public void setConnectType(String connectType) {
        this.connectType = connectType;
    }

    public String getPriceTableType() {
        return priceTableType;
    }

    public void setPriceTableType(String priceTableType) {
        this.priceTableType = priceTableType;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Integer getRowStart() {
        return rowStart;
    }

    public void setRowStart(Integer rowStart) {
        this.rowStart = rowStart;
    }

    public Integer getRowEnd() {
        return rowEnd;
    }

    public void setRowEnd(Integer rowEnd) {
        this.rowEnd = rowEnd;
    }
}
