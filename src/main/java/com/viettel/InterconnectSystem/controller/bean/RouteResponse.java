package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.Route;

public class RouteResponse {

    private Route route;
    private String partnerName;
    private String switchName;
    private String prefixNumber;
    private String serviceName;

    public RouteResponse(Route route, String partnerName, String switchName, String prefixNumber, String serviceName) {
        this.route = route;
        this.partnerName = partnerName;
        this.switchName = switchName;
        this.prefixNumber = prefixNumber;
        this.serviceName = serviceName;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }

    public String getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(String prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
