package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.utils.ArrayUtils;

import java.util.Date;

public class PriceInfo {

    private String[] areaCodes;
    private Double rate;
    private Date rateValid;

    public PriceInfo(String[] areaCodes, Double rate, Date rateValid) {
        this.areaCodes = areaCodes;
        this.rate = rate;
        this.rateValid = rateValid;
    }

    public void addAreaCode(String[] areaCodes){
        this.areaCodes = ArrayUtils.concat(this.areaCodes, areaCodes);
    }

    public String[] getAreaCodes() {
        return areaCodes;
    }

    public void setAreaCodes(String[] areaCodes) {
        this.areaCodes = areaCodes;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Date getRateValid() {
        return rateValid;
    }

    public void setRateValid(Date rateValid) {
        this.rateValid = rateValid;
    }
}
