package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.StandardizeProcessParam;

public class StandardizeProcessParamResponse {

    private StandardizeProcessParam standardizeProcessParam;
    private String switchName;

    public StandardizeProcessParamResponse(StandardizeProcessParam standardizeProcessParam, String switchName) {
        this.standardizeProcessParam = standardizeProcessParam;
        this.switchName = switchName;
    }

    public StandardizeProcessParam getStandardizeProcessParam() {
        return standardizeProcessParam;
    }

    public void setStandardizeProcessParam(StandardizeProcessParam standardizeProcessParam) {
        this.standardizeProcessParam = standardizeProcessParam;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }
}
