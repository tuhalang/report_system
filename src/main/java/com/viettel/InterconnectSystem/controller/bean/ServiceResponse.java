package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.Service;

public class ServiceResponse {
    private Service service;
    private String voiceBlockCode;
    private String switchName;
    private String partnerName;

    public ServiceResponse(Service service, String voiceBlockCode, String switchName, String partnerName) {
        this.service = service;
        this.voiceBlockCode = voiceBlockCode;
        this.switchName = switchName;
        this.partnerName = partnerName;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public String getVoiceBlockCode() {
        return voiceBlockCode;
    }

    public void setVoiceBlockCode(String voiceBlockCode) {
        this.voiceBlockCode = voiceBlockCode;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }
}
