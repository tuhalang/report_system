package com.viettel.InterconnectSystem.controller.bean;

import com.viettel.InterconnectSystem.model.htds.GetProcessParam;

public class GetProcessParamResponse {

    private GetProcessParam getProcessParam;
    private String switchName;

    public GetProcessParamResponse(GetProcessParam getProcessParam, String switchName) {
        this.getProcessParam = getProcessParam;
        this.switchName = switchName;
    }

    public GetProcessParam getGetProcessParam() {
        return getProcessParam;
    }

    public void setGetProcessParam(GetProcessParam getProcessParam) {
        this.getProcessParam = getProcessParam;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }
}
