package com.viettel.InterconnectSystem.controller.bean;

import java.util.LinkedHashMap;

public class RequestBean {
    private Integer pageNumber;
    private Integer numOfRecord;
    private LinkedHashMap<String, Object> wsRequest;

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getNumOfRecord() {
        return numOfRecord;
    }

    public void setNumOfRecord(Integer numOfRecord) {
        this.numOfRecord = numOfRecord;
    }

    public LinkedHashMap<String, Object> getWsRequest() {
        return wsRequest;
    }

    public void setWsRequest(LinkedHashMap<String, Object> wsRequest) {
        this.wsRequest = wsRequest;
    }
}
