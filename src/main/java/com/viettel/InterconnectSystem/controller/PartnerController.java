package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Partner;
import com.viettel.InterconnectSystem.service.PartnerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class PartnerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartnerController.class);

    @Autowired
    private PartnerService partnerService;


    @RequestMapping(value = "/partners", method = RequestMethod.GET)
    public ResponseEntity getPartners(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    partnerService.findAll(pageable, responseBean, deleted);
                }else{
                    partnerService.searchByPartName(pageable,responseBean,key,deleted);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/partners", method = RequestMethod.PUT)
    public ResponseEntity updatePartner(@RequestBody Partner partner){
        ResponseBean responseBean = new ResponseBean();
        try {
            partnerService.update(partner, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/partners", method = RequestMethod.DELETE)
    public ResponseEntity deletePartner(@RequestParam Long partnerId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            partnerService.delete(partnerId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/partners", method = RequestMethod.POST)
    public ResponseEntity createPartner(@RequestBody Partner partner){
        ResponseBean responseBean = new ResponseBean();
        try {
            partnerService.create(partner, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
