package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.service.ReportService;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.util.Date;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/reports")
public class ReportController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

    @Autowired
    private ReportService reportService;

    @RequestMapping(value = "/inter-price", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] getInterPrice(@RequestParam Long partnerId,
                                @RequestParam String interType,
                                @RequestParam String direction){
        ResponseBean responseBean = new ResponseBean();
        try {
            ByteArrayInputStream byteArrayInputStream = reportService.getInterPrice(partnerId, interType, direction);
            return IOUtils.toByteArray(byteArrayInputStream);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
            return null;
        }
    }

    @RequestMapping(value = "/report", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public byte[] getReportData(@RequestParam Long categoryId,
                                @RequestParam(required = false) Long partnerId,
                                @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) {
        ResponseBean responseBean = new ResponseBean();
        try {
            ByteArrayInputStream byteArrayInputStream = reportService.getReportData(categoryId, partnerId, date);
            return IOUtils.toByteArray(byteArrayInputStream);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
            return null;
        }
    }
}
