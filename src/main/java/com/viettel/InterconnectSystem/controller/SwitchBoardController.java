package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SwitchBoard;
import com.viettel.InterconnectSystem.service.SwitchBoardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class SwitchBoardController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SwitchBoardController.class);

    @Autowired
    private SwitchBoardService switchBoardService;


    @RequestMapping(value = "/switchboards", method = RequestMethod.GET)
    public ResponseEntity getServices(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    switchBoardService.findAll(pageable, responseBean, deleted);
                }else{
                    switchBoardService.searchByIsDeleteAndSwitchName(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/switchboards", method = RequestMethod.PUT)
    public ResponseEntity updateService(@RequestBody SwitchBoard switchBoard){
        ResponseBean responseBean = new ResponseBean();
        try {
            switchBoardService.update(switchBoard, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/switchboards", method = RequestMethod.DELETE)
    public ResponseEntity deletePartner(@RequestParam Long switchId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            switchBoardService.delete(switchId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/switchboards", method = RequestMethod.POST)
    public ResponseEntity createPartner(@RequestBody SwitchBoard switchBoard){
        ResponseBean responseBean = new ResponseBean();
        try {
            switchBoardService.create(switchBoard, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
