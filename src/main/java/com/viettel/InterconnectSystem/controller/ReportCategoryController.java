package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.inter.ReportCategory;
import com.viettel.InterconnectSystem.service.ReportCategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/reports")
public class ReportCategoryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportCategoryController.class);

    @Autowired
    private ReportCategoryService reportCategoryService;


    @RequestMapping(value = "/report-categories", method = RequestMethod.GET)
    public ResponseEntity getReportCategories(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    reportCategoryService.findAll(pageable, responseBean, deleted);
                }else{
                    reportCategoryService.searchByReportName(pageable,responseBean,key,deleted);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/report-categories", method = RequestMethod.PUT)
    public ResponseEntity updateReportCategory(@RequestBody ReportCategory reportCategory){
        ResponseBean responseBean = new ResponseBean();
        try {
            reportCategoryService.update(reportCategory, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/report-categories", method = RequestMethod.DELETE)
    public ResponseEntity deleteReportCategory(@RequestParam Long reportId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            reportCategoryService.delete(reportId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/report-categories", method = RequestMethod.POST)
    public ResponseEntity createReportCategory(@RequestBody ReportCategory reportCategory){
        ResponseBean responseBean = new ResponseBean();
        try {
            reportCategoryService.create(reportCategory, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
