package com.viettel.InterconnectSystem.controller;


import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SmsCenter;
import com.viettel.InterconnectSystem.service.SmsCenterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class SmsCenterController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsCenterController.class);

    @Autowired
    private SmsCenterService smsCenterService;


    @RequestMapping(value = "/smscenters", method = RequestMethod.GET)
    public ResponseEntity getSmsCenters(@RequestParam Integer deleted, @RequestParam Integer page,
                                     @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    smsCenterService.findAll(pageable, responseBean, deleted);
                }else{
                    smsCenterService.searchByIsDeleteAndPartType(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/smscenters", method = RequestMethod.PUT)
    public ResponseEntity updateSmsCenter(@RequestBody SmsCenter smsCenter){
        ResponseBean responseBean = new ResponseBean();
        try {
            smsCenterService.update(smsCenter, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/smscenters", method = RequestMethod.DELETE)
    public ResponseEntity deletePartner(@RequestParam Long smsCenterId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            smsCenterService.delete(smsCenterId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/smscenters", method = RequestMethod.POST)
    public ResponseEntity createPartner(@RequestBody SmsCenter smsCenter){
        ResponseBean responseBean = new ResponseBean();
        try {
            smsCenterService.create(smsCenter, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
