package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.AuthenticationRequest;
import com.viettel.InterconnectSystem.controller.bean.ChangePassBean;
import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.controller.bean.SignUpRequest;
import com.viettel.InterconnectSystem.model.htds.User;
import com.viettel.InterconnectSystem.repository.htds.UserRepository;
import com.viettel.InterconnectSystem.security.jwt.JwtTokenProvider;
import com.viettel.InterconnectSystem.utils.WebserviceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

@CrossOrigin("*")
@RestController
@PropertySource({"classpath:application.properties"})
@RequestMapping("/auth")
public class AuthenticationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    Environment env;


    @RequestMapping(value = "/users", method = RequestMethod.DELETE)
    public ResponseEntity deleted(@RequestParam String username, @RequestParam Integer deleted, HttpServletRequest request){
        String token = jwtTokenProvider.resolveToken(request);
        ResponseBean responseBean = new ResponseBean();
        if(jwtTokenProvider.validateToken(token)){
            String role = jwtTokenProvider.getRole(token);
            if(role.equalsIgnoreCase("ADMIN")){
                User user = userRepository.findByUsername(username).orElse(null);
                if(user != null){
                    user.setActive(deleted==1);
                    userRepository.save(user);
                    responseBean.setErrorCode("0");
                    responseBean.setMsg("Successfully");
                    return ResponseEntity.ok(responseBean);
                }
            }
        }
        return ResponseEntity.ok(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/user/reset_pass", method = RequestMethod.POST)
    public ResponseEntity resetPass(@RequestBody AuthenticationRequest auth, HttpServletRequest request){
        String token = jwtTokenProvider.resolveToken(request);
        ResponseBean responseBean = new ResponseBean();
        if(jwtTokenProvider.validateToken(token)){
            String role = jwtTokenProvider.getRole(token);
            if(role.equalsIgnoreCase("ADMIN")){
                User user = userRepository.findByUsername(auth.getUsername()).orElse(null);
                if(user != null){
                    user.setPassword(passwordEncoder.encode(auth.getPassword()));
                    userRepository.save(user);
                    responseBean.setErrorCode("0");
                    responseBean.setMsg("Successfully");
                    return ResponseEntity.ok(responseBean);
                }
            }
        }
        return ResponseEntity.ok(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ResponseEntity getAllUser(HttpServletRequest request){
        String token = jwtTokenProvider.resolveToken(request);
        ResponseBean responseBean = new ResponseBean();
        if(jwtTokenProvider.validateToken(token)){
            String role = jwtTokenProvider.getRole(token);
            if(role.equalsIgnoreCase("ADMIN")){
                List<User> users = userRepository.findAllNotAdmin();
                LinkedHashMap<String, Object> datas = new LinkedHashMap<>();
                datas.put("items", users);
                responseBean.setErrorCode("0");
                responseBean.setMsg("Successfully");
                responseBean.setData(datas);
                return ResponseEntity.ok(responseBean);
            }
        }
        return ResponseEntity.ok(HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(value = "/user/change_pass", method = RequestMethod.POST)
    public ResponseEntity changePass(@RequestBody ChangePassBean changePassBean, HttpServletRequest request){
        String token = jwtTokenProvider.resolveToken(request);
        String oldPass = changePassBean.getOldPass();
        String pass = changePassBean.getPass();
        String checkPass = changePassBean.getCheckPass();
        ResponseBean response = new ResponseBean();
        if(jwtTokenProvider.validateToken(token)){
            String username = jwtTokenProvider.getUsername(token);
            User user = userRepository.findByUsername(username).orElse(null);
            if(StringUtils.isEmpty(oldPass) || StringUtils.isEmpty(pass)
                    || StringUtils.isEmpty(checkPass) || !pass.equals(checkPass)){
                response.setErrorCode("1");
                response.setMsg("Invalid input");
                return ResponseEntity.ok(response);
            }else{
                if(!passwordEncoder.matches(oldPass, user.getPassword())){
                    response.setErrorCode("1");
                    response.setMsg("Old Password is not correct !");
                    return ResponseEntity.ok(response);
                }
                user.setPassword(passwordEncoder.encode(pass));
                userRepository.save(user);
                response.setErrorCode("0");
                response.setMsg("Change pass successfully !");
                return ResponseEntity.ok(response);
            }
        }else{
            return ResponseEntity.ok(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/user/info", method = RequestMethod.GET)
    public ResponseEntity getInfo(@RequestParam String token){
        ResponseBean responseBean = new ResponseBean();
        if(jwtTokenProvider.validateToken(token)){
            String username = jwtTokenProvider.getUsername(token);
            User user = userRepository.findByUsername(username).orElse(null);
            LinkedHashMap<String, Object> data = new LinkedHashMap<>();
            data.put("roles", Arrays.asList("admin"));
            data.put("introduction", "I'm administrator");
            data.put("avatar", "");
            data.put("name", username);

            responseBean.setErrorCode("0");
            responseBean.setMsg("Successfully !");
            responseBean.setData(data);
        }else{
            responseBean.setErrorCode("1");
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public ResponseEntity signin(@RequestBody AuthenticationRequest data){
        ResponseBean responseBean = new ResponseBean();
        try {
            String username = data.getUsername();
            String password = data.getPassword();
            String role = WebserviceUtils.loginPassport(username,password,env);
            if(StringUtils.isEmpty(role)){
                LOGGER.error("Invalid username/password supplied");
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid username/password supplied");
                responseBean.setData(null);
            }else{
                String token = jwtTokenProvider.createToken(username, role);
                responseBean.setErrorCode("0");
                responseBean.setMsg("SignIn successfully !");
                LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
                wsResponse.put("username", username);
                wsResponse.put("token", token);
                wsResponse.put("role", role);
                responseBean.setData(wsResponse);
            }
        } catch (AuthenticationException e) {
            LOGGER.error("Invalid username/password supplied");
            responseBean.setErrorCode("1");
            responseBean.setMsg("Invalid username/password supplied");
            responseBean.setData(null);
        } catch (Exception e){
            LOGGER.error(e.getMessage() , e);
            responseBean.setErrorCode("1");
            responseBean.setMsg("Server error !");
            responseBean.setData(null);
        }
        return ResponseEntity.ok(responseBean);
    }

    @CrossOrigin
    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity signup(@RequestBody SignUpRequest data){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(data.getPassword().equals(data.getValidPassword())){
                User user = new User();
                user.setUserId(UUID.randomUUID().toString());
                user.setActive(true);
                user.setUsername(data.getUsername());
                user.setPassword(passwordEncoder.encode(data.getPassword()));
                userRepository.save(user);
                responseBean.setErrorCode("0");
                responseBean.setMsg("SignUp successfully !");
                responseBean.setData(null);
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage() , e);
            responseBean.setErrorCode("1");
            responseBean.setMsg("Server error !");
            responseBean.setData(null);
        }
        return ResponseEntity.ok(responseBean);
    }
}
