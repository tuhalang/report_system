package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Country;
import com.viettel.InterconnectSystem.service.CountryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class CountryController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountryController.class);

    @Autowired
    private CountryService countryService;

    @RequestMapping(value = "/countries", method = RequestMethod.GET)
    public ResponseEntity getCountries(@RequestParam Integer deleted, @RequestParam Integer page,
                                       @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page - 1, limit);
                if(key == null) {
                    countryService.findAll(pageable, responseBean, deleted);
                }else{
                    countryService.searchByCountryName(pageable, responseBean, key, deleted);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/countries/search", method = RequestMethod.GET)
    public ResponseEntity searchCountries(@RequestParam Integer deleted, @RequestParam Integer page,
                                          @RequestParam Integer limit, @RequestParam String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                countryService.searchByCountryName(pageable, responseBean, key, deleted);
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/countries", method = RequestMethod.PUT)
    public ResponseEntity updateCountry(@RequestBody Country country){
        ResponseBean responseBean = new ResponseBean();
        try {
            countryService.update(country, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/countries", method = RequestMethod.DELETE)
    public ResponseEntity deleteCountry(@RequestParam Long countryId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            countryService.delete(countryId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/countries", method = RequestMethod.POST)
    public ResponseEntity createCountry(@RequestBody Country country){
        ResponseBean responseBean = new ResponseBean();
        try {
            countryService.create(country, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

}
