package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.ConvertProcessParam;
import com.viettel.InterconnectSystem.service.ConvertProcessParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class ConvertProcessParamController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConvertProcessParamController.class);

    @Autowired
    private ConvertProcessParamService convertProcessParamService;


    @RequestMapping(value = "/convert-process-params", method = RequestMethod.GET)
    public ResponseEntity getProcessParam(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    convertProcessParamService.findAll(pageable, responseBean, deleted);
                }else{
                    convertProcessParamService.searchByIsDeleteAndProcessCode(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/convert-process-params", method = RequestMethod.PUT)
    public ResponseEntity updateProcessParam(@RequestBody ConvertProcessParam convertProcessParam){
        ResponseBean responseBean = new ResponseBean();
        try {
            convertProcessParamService.update(convertProcessParam, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/convert-process-params", method = RequestMethod.DELETE)
    public ResponseEntity deleteProcessParam(@RequestParam Long convertId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            convertProcessParamService.delete(convertId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/convert-process-params", method = RequestMethod.POST)
    public ResponseEntity createProcessParam(@RequestBody ConvertProcessParam convertProcessParam){
        ResponseBean responseBean = new ResponseBean();
        try {
            convertProcessParamService.create(convertProcessParam, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
