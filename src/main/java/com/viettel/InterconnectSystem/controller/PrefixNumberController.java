package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.PrefixNumber;
import com.viettel.InterconnectSystem.service.PrefixNumberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class PrefixNumberController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RouteController.class);

    @Autowired
    private PrefixNumberService prefixNumberService;


    @RequestMapping(value = "/prefix-numbers", method = RequestMethod.GET)
    public ResponseEntity getPrefixNumber(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    prefixNumberService.findAll(pageable, responseBean, deleted);
                }else{
                    prefixNumberService.searchByIsDeleteAndPrefixNumber(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/prefix-numbers", method = RequestMethod.PUT)
    public ResponseEntity updatePrefixNumber(@RequestBody PrefixNumber prefixNumber){
        ResponseBean responseBean = new ResponseBean();
        try {
            prefixNumberService.update(prefixNumber, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/prefix-numbers", method = RequestMethod.DELETE)
    public ResponseEntity deletePrefixNumber(@RequestParam Long prefixNumberId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            prefixNumberService.delete(prefixNumberId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/prefix-numbers", method = RequestMethod.POST)
    public ResponseEntity createPrefixNumber(@RequestBody PrefixNumber prefixNumber){
        ResponseBean responseBean = new ResponseBean();
        try {
            prefixNumberService.create(prefixNumber, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
