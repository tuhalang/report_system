package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.service.AreaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class AreaController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AreaController.class);

    @Autowired
    private AreaService areaService;

    @RequestMapping(value = "/areas", method = RequestMethod.GET)
    public ResponseEntity getAreas(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    areaService.searchByIsDeleteAndName(pageable,responseBean,deleted, "");
                }else{
                    areaService.searchByIsDeleteAndName(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
