package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.service.VoiceBlockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class VoiceBlockController {

    private static final Logger LOGGER = LoggerFactory.getLogger(VoiceBlockController.class);

    @Autowired
    private VoiceBlockService voiceBlockService;

    @RequestMapping(value = "/voice-blocks", method = RequestMethod.GET)
    public ResponseEntity getVoiceBlocks(@RequestParam Integer deleted, @RequestParam Integer page,
                                   @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    voiceBlockService.searchByIsDeleteAndBlockName(pageable,responseBean,deleted, "");
                }else{
                    voiceBlockService.searchByIsDeleteAndBlockName(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
