package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SMPPCP;
import com.viettel.InterconnectSystem.service.SMPPCPService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class SmppCpController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmppCpController.class);

    @Autowired
    private SMPPCPService smppcpService;


    @RequestMapping(value = "/smppcps", method = RequestMethod.GET)
    public ResponseEntity getSMPPCPS(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    smppcpService.findAll(pageable, responseBean, deleted);
                }else{
                    smppcpService.searchByIsDeleteAndSmppCpName(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/smppcps", method = RequestMethod.PUT)
    public ResponseEntity updateSMPPCP(@RequestBody SMPPCP smppcp){
        ResponseBean responseBean = new ResponseBean();
        try {
            smppcpService.update(smppcp, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/smppcps", method = RequestMethod.DELETE)
    public ResponseEntity deletePartner(@RequestParam Long smppCpId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            smppcpService.delete(smppCpId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/smppcps", method = RequestMethod.POST)
    public ResponseEntity createPartner(@RequestBody SMPPCP smppcp){
        ResponseBean responseBean = new ResponseBean();
        try {
            smppcpService.create(smppcp, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
