package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.StandardizeProcessParam;
import com.viettel.InterconnectSystem.service.StandardizeProcessParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class StandardizeProcessParamController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StandardizeProcessParamController.class);

    @Autowired
    private StandardizeProcessParamService standardizeProcessParamService;


    @RequestMapping(value = "/standardize-process-params", method = RequestMethod.GET)
    public ResponseEntity getProcessParam(@RequestParam Integer deleted, @RequestParam Integer page,
                                          @RequestParam Integer limit, @RequestParam(required = false) String key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    standardizeProcessParamService.findAll(pageable, responseBean, deleted);
                }else{
                    standardizeProcessParamService.searchByIsDeleteAndProcessCode(pageable,responseBean,deleted, key);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/standardize-process-params", method = RequestMethod.PUT)
    public ResponseEntity updateProcessParam(@RequestBody StandardizeProcessParam standardizeProcessParam){
        ResponseBean responseBean = new ResponseBean();
        try {
            standardizeProcessParamService.update(standardizeProcessParam, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/standardize-process-params", method = RequestMethod.DELETE)
    public ResponseEntity deleteProcessParam(@RequestParam Long standardizeId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            standardizeProcessParamService.delete(standardizeId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/standardize-process-params", method = RequestMethod.POST)
    public ResponseEntity createProcessParam(@RequestBody StandardizeProcessParam standardizeProcessParam){
        ResponseBean responseBean = new ResponseBean();
        try {
            standardizeProcessParamService.create(standardizeProcessParam, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
