package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Currency;
import com.viettel.InterconnectSystem.service.CurrencyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedHashMap;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/components")
public class CurrencyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrencyController.class);

    @Autowired
    private CurrencyService currencyService;


    @RequestMapping(value = "/currencies", method = RequestMethod.GET)
    public ResponseEntity findAll(){
        ResponseBean responseBean = new ResponseBean();
        try{
            List<Currency> currencies = currencyService.findAll();
            LinkedHashMap<String , Object> hashMap = new LinkedHashMap<>();
            hashMap.put("items", currencies);
            responseBean.setErrorCode("0");
            responseBean.setMsg("Successfully !");
            responseBean.setData(hashMap);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
