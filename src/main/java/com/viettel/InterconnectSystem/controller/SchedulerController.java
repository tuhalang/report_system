package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.inter.Scheduler;
import com.viettel.InterconnectSystem.service.SchedulerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/reports")
public class SchedulerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerController.class);

    @Autowired
    private SchedulerService schedulerService;


    @RequestMapping(value = "/schedulers", method = RequestMethod.GET)
    public ResponseEntity getSchedulers(@RequestParam Integer deleted, @RequestParam Integer page,
                                      @RequestParam Integer limit, @RequestParam(required = false) Long key){
        ResponseBean responseBean = new ResponseBean();
        try{
            if(page != null && limit != null && deleted != null) {
                Pageable pageable = PageRequest.of(page-1, limit);
                if(key == null) {
                    schedulerService.findAll(pageable, responseBean, deleted);
                }else{
                    schedulerService.searchBySynthTypeId(pageable,responseBean, key, deleted);
                }
            }else {
                responseBean.setErrorCode("1");
                responseBean.setMsg("Invalid params !");
            }
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }

    @RequestMapping(value = "/schedulers", method = RequestMethod.PUT)
    public ResponseEntity updateScheduler(@RequestBody Scheduler scheduler){
        ResponseBean responseBean = new ResponseBean();
        try {
            schedulerService.update(scheduler, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/schedulers", method = RequestMethod.DELETE)
    public ResponseEntity deleteScheduler(@RequestParam Long synthTypeId, @RequestParam Integer deleted){
        ResponseBean responseBean = new ResponseBean();
        try {
            schedulerService.delete(synthTypeId, deleted, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
    @RequestMapping(value = "/schedulers", method = RequestMethod.POST)
    public ResponseEntity createScheduler(@RequestBody Scheduler scheduler){
        ResponseBean responseBean = new ResponseBean();
        try {
            schedulerService.create(scheduler, responseBean);
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg(e.getMessage());
        }
        return ResponseEntity.ok(responseBean);
    }
}
