package com.viettel.InterconnectSystem.controller;

import com.viettel.InterconnectSystem.controller.bean.FileUpload;
import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.service.ImportService;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/import")
@PropertySource(value = {"classpath:application.properties"})
public class FileUploadController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);

    @Autowired
    private ImportService importService;

    @Autowired
    Environment environment;

    private String getFilename(String name){
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-YYYY");
        String prefix = df.format(new Date());
        return prefix + "-" + name;
    }

    @RequestMapping(value = "/template", method = RequestMethod.GET)
    public byte[] getTemplateFile(){
        try {
            String path = environment.getProperty("template.file.path");
            File file = new File(path);
            if (file.exists()) {
                InputStream inputStream = new FileInputStream(file);
                return IOUtils.toByteArray(inputStream);
            }
            return null;
        }catch (Exception e){
            return null;
        }
    }


    @RequestMapping(value = "/prices", method = RequestMethod.POST)
    public ResponseEntity importPrices(@RequestParam MultipartFile fileUpload, @RequestParam String interType,
                                       @RequestParam Long partnerId, @RequestParam String partType,
                                       @RequestParam(required = false) Integer isDelete, @RequestParam String direction,
                                       @RequestParam String codePriceName, @RequestParam String connectType,
                                       @RequestParam(required = false) String priceTableType, @RequestParam Integer destination,
                                       @RequestParam Integer areaCode, @RequestParam Integer rate,
                                       @RequestParam Integer rateValid, @RequestParam Integer rowStart,
                                       @RequestParam Integer rowEnd, @RequestParam Long currencyId){
        ResponseBean responseBean = new ResponseBean();
        try{
            String uploadFolder = environment.getProperty("folder.path");
            byte[] bytes = fileUpload.getBytes();
            String filename = getFilename(fileUpload.getOriginalFilename());
            Path path = Paths.get(uploadFolder, filename);
            Files.write(path, bytes);
            File file = new File(path.toUri());

            FileUpload obj = new FileUpload(interType, partnerId, partType, new Date(), isDelete, direction, codePriceName,
                    connectType, priceTableType, file, destination, areaCode, rate, rateValid, rowStart, rowEnd, currencyId);
            importService.importPrice(obj);
            responseBean.setErrorCode("0");
            responseBean.setMsg("Successfully !");
        }catch (Exception e){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Unsuccessfully ! " + e.getMessage());
            LOGGER.error(e.getMessage(), e);
        }
        return ResponseEntity.ok(responseBean);
    }
}
