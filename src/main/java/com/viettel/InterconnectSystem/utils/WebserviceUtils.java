package com.viettel.InterconnectSystem.utils;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class WebserviceUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebserviceUtils.class);
    private static final String ROLE_REPORT = "ROLE_INTER_REPORT";
    private static final String ROLE_STAFF = "ROLE_INTER_VIEW";
    private static final String ROLE_ADMIN = "ROLE_INTER_ADMIN";
    private static final Integer TIMEOUT = 120000;

    private static String rawLogin = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pas=\"http://passport.viettel.com/\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <pas:validate>\n" +
            "         <!--Optional:-->\n" +
            "         <userName>@username</userName>\n" +
            "         <!--Optional:-->\n" +
            "         <password>@password</password>\n" +
            "         <!--Optional:-->\n" +
            "         <domainCode>DMWeb</domainCode>\n" +
            "      </pas:validate>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>";

    private static String readFile(String path, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static void main(String[] args) throws IOException {
        loginPassport("1", "2", null);
    }

    private static String getElementXML(Document doc, String xpath) throws XPathExpressionException, TransformerException {
        if (doc == null)
            return null;
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList = (NodeList) xPath.compile(xpath).evaluate(doc, XPathConstants.NODESET);
        if (nodeList != null && nodeList.getLength() > 0) {
            StringWriter writer = new StringWriter();
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(new DOMSource(nodeList.item(0)), new StreamResult(writer));
            String xml = writer.toString();
            return xml;
        }
        return null;
    }

    private static String getTextContent(Document doc, String xpath) throws XPathExpressionException {
        if (doc == null)
            return null;

        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList = (NodeList) xPath.compile(xpath).evaluate(doc, XPathConstants.NODESET);
        if (nodeList != null && nodeList.getLength() > 0) {
            return nodeList.item(0).getTextContent();
        }
        return null;
    }


    public static String loginPassport(String username, String password, Environment env) {
        return "ADMIN";
//        try {
//            String wsUrl = env.getProperty("url.passport");
//            String inputXML = rawLogin.replace("@username", username);
//            inputXML = inputXML.replace("@password", password);
//            String soapAction = "POST";
//            String xmlOut = callWS(wsUrl, inputXML, soapAction, 1, 120000);
//
//            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
//            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
//            factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
//            factory.setXIncludeAware(false);
//            factory.setExpandEntityReferences(false);
//
//            DocumentBuilder builder = factory.newDocumentBuilder();
//            InputSource is = new InputSource(new StringReader(xmlOut));
//            Document doc = builder.parse(is);
//
//
//            String originalReturn = getTextContent(doc, "/Envelope/Body/validateResponse/return");
//            InputStream is1 = new ByteArrayInputStream(originalReturn.getBytes());
//            Document orgDoc = builder.parse(is1);
//
//            JSONObject jsonObject = XML.toJSONObject(getElementXML(orgDoc, "/Results/Roles"), true);
//            JSONObject obj = jsonObject.getJSONObject("Roles");
//            Object roleObj = obj.get("Row");
//            boolean roleReport = false;
//            boolean roleStaff = false;
//            boolean roleAdmin = false;
//            if(roleObj instanceof JSONArray) {
//                JSONArray arrRole = (JSONArray) roleObj;
//                for (int i = 0; i < arrRole.length(); i++) {
//                    JSONObject role = arrRole.getJSONObject(i);
//                    if (role.get("ROLE_CODE").toString().equalsIgnoreCase(ROLE_REPORT)) {
//                        roleReport = true;
//                    }
//                    if (role.get("ROLE_CODE").toString().equalsIgnoreCase(ROLE_STAFF)) {
//                        roleStaff = true;
//                    }
//                    if (role.get("ROLE_CODE").toString().equalsIgnoreCase(ROLE_ADMIN)) {
//                        roleAdmin = true;
//                    }
//                }
//            }else if(roleObj instanceof JSONObject){
//                JSONObject role = (JSONObject) roleObj;
//                if (role.get("ROLE_CODE").toString().equalsIgnoreCase(ROLE_REPORT)) {
//                    roleReport = true;
//                }
//                if (role.get("ROLE_CODE").toString().equalsIgnoreCase(ROLE_STAFF)) {
//                    roleStaff = true;
//                }
//                if (role.get("ROLE_CODE").toString().equalsIgnoreCase(ROLE_ADMIN)) {
//                    roleAdmin = true;
//                }
//            }
//            String roleName = "";
//            if(roleAdmin){
//                roleName = "ADMIN";
//                return roleName;
//            }
//
//            if(roleReport){
//                roleName = "REPORT";
//                return roleName;
//            }
//
//            if(roleStaff){
//                roleName = "STAFF";
//                return roleName;
//            }
//
//            return roleName;
//        } catch (Exception e) {
//            LOGGER.error(e.getMessage(), e);
//            return "";
//        }

    }

    private  static String callWS(String wsURL, String inputXML, String soapAction) throws IOException {
        PostMethod post = new PostMethod(wsURL);
        RequestEntity entity = new StringRequestEntity(inputXML, "text/xml", "UTF-8");
        post.setRequestEntity(entity);
        post.setRequestHeader("SOAPAction",soapAction);
        HttpClient httpclient = new HttpClient();
        httpclient.executeMethod(post);
        HttpConnectionManager conMgr = httpclient.getHttpConnectionManager();
        HttpConnectionManagerParams conPars = conMgr.getParams();
        conPars.setConnectionTimeout(120000);
        conPars.setSoTimeout(120000);
        String responseBody = post.getResponseBodyAsString();
        //LOGGER.info("RESPONSE: " + responseBody);
        return responseBody;
    }

    private static String callWS(String wsURL, String inputXML, String soapAction, int retry, int timeout) {
        String result = "";
        String responseString;

        for (int i = 0; i < retry; i++) {
            //LOGGER.info("Try " + i + " Call WS got XML input: " + inputXML);
            try {
                URL url = new URL(wsURL);
                URLConnection connection = url.openConnection();
                HttpURLConnection httpConn = (HttpURLConnection) connection;
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                bout.write(inputXML.getBytes());

                byte[] b = bout.toByteArray();
                // Set the appropriate HTTP parameters.
                httpConn.setReadTimeout(timeout); // ms
                httpConn.setConnectTimeout(timeout); // ms
                httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
                httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                httpConn.setRequestProperty("SOAPAction", soapAction);

                httpConn.setRequestMethod("POST");
                httpConn.setDoOutput(true);
                httpConn.setDoInput(true);
                OutputStream out = httpConn.getOutputStream();
                //Write the content of the request to the outputstream of the HTTP Connection.
                out.write(b);
                out.close();
                //Ready with sending the request.
                //Read the response.
                InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
                BufferedReader in = new BufferedReader(isr);

                //Write the SOAP message response to a String.
                while ((responseString = in.readLine()) != null) {
                    result += responseString;
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage(), ex);
                result = "";
            }

            if (result != null && !result.isEmpty()) {
                break;
            }
        }
        LOGGER.info("Call WS got Response: '" + result + "'");
        return result;
    }
}
