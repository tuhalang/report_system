package com.viettel.InterconnectSystem.utils;

import com.viettel.InterconnectSystem.model.htds.view.FullPreInterNumber;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ExcelUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtils.class);

    public static ByteArrayInputStream generateInterPrice(String[] headers, List<FullPreInterNumber> datas) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet reportSheet = workbook.createSheet();

        Row rowHeaders = reportSheet.createRow(0);

        // Style header
        CellStyle headerStyle = workbook.createCellStyle();
        //headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        //headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // Font header
        XSSFFont fontHeader = ((XSSFWorkbook) workbook).createFont();
        fontHeader.setFontName("Arial");
        fontHeader.setFontHeightInPoints((short) 12);
        fontHeader.setBold(true);
        headerStyle.setFont(fontHeader);



        // Body style
        XSSFFont fontBody = ((XSSFWorkbook) workbook).createFont();
        fontBody.setFontName("Arial");
        fontBody.setFontHeightInPoints((short) 12);
        fontBody.setBold(false);

        CellStyle bodyStyle = workbook.createCellStyle();
        bodyStyle.setFont(fontBody);
        bodyStyle.setWrapText(true);

        for(int row=0; row<datas.size(); row++){
            Row rowBody = reportSheet.createRow(row+1);

            Object[] objs = new Object[4];
            objs[0] = datas.get(row).getNumberGroupName();
            objs[1] = datas.get(row).getPrefixNumber();
            objs[2] = datas.get(row).getPrice();
            objs[3] = datas.get(row).getDateApply();

            for(int col=0; col<objs.length; col++){
                Cell bodyCell = rowBody.createCell(col);
                Object value = objs[col];
                if(value != null)
                    if(value instanceof BigDecimal){
                        BigDecimal bValue = (BigDecimal) value;
                        bodyCell.setCellType(CellType.NUMERIC);
                        bodyCell.setCellValue(bValue.doubleValue());
                    }else if(value instanceof Integer || value instanceof Double){
                        bodyCell.setCellType(CellType.NUMERIC);
                        bodyCell.setCellValue((Double) value);
                    }else if(value instanceof Date){
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                        Date date = (Date) value;
                        bodyCell.setCellType(CellType.STRING);
                        bodyCell.setCellValue(sdf.format(date));
                    }else {
                        bodyCell.setCellType(CellType.STRING);
                        bodyCell.setCellValue(objs[col].toString());
                    }
                else
                    bodyCell.setCellValue("");
                bodyCell.setCellStyle(bodyStyle);
                //reportSheet.autoSizeColumn(col);
            }
        }

        for(int i=0; i<headers.length; i++){
            Cell headerCell = rowHeaders.createCell(i);
            headerCell.setCellValue(headers[i]);
            headerCell.setCellStyle(headerStyle);
            reportSheet.autoSizeColumn(i);
        }


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        return new ByteArrayInputStream(outputStream.toByteArray());
    }

    public static ByteArrayInputStream generateExcelFile(String[] headers, List<Object[]> datas) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet reportSheet = workbook.createSheet();

        Row rowHeaders = reportSheet.createRow(0);

        // Style header
        CellStyle headerStyle = workbook.createCellStyle();
        //headerStyle.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        //headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        // Font header
        XSSFFont fontHeader = ((XSSFWorkbook) workbook).createFont();
        fontHeader.setFontName("Arial");
        fontHeader.setFontHeightInPoints((short) 12);
        fontHeader.setBold(true);
        headerStyle.setFont(fontHeader);



        // Body style
        XSSFFont fontBody = ((XSSFWorkbook) workbook).createFont();
        fontBody.setFontName("Arial");
        fontBody.setFontHeightInPoints((short) 12);
        fontBody.setBold(false);

        CellStyle bodyStyle = workbook.createCellStyle();
        bodyStyle.setFont(fontBody);
        bodyStyle.setWrapText(true);

        for(int row=0; row<datas.size(); row++){
            Row rowBody = reportSheet.createRow(row+1);
            Object[] objs = datas.get(row);
            for(int col=0; col<objs.length; col++){
                Cell bodyCell = rowBody.createCell(col);
                Object value = objs[col];
                if(value != null)
                    if(value instanceof BigDecimal){
                        BigDecimal bValue = (BigDecimal) value;
                        bodyCell.setCellValue(bValue.doubleValue());
                    }else if(value instanceof Integer || value instanceof Double){
                        bodyCell.setCellValue((Double) value);
                    }else if(value instanceof Date){
                        bodyCell.setCellValue((Date) value);
                    }else {
                        bodyCell.setCellValue(objs[col].toString());
                    }
                else
                    bodyCell.setCellValue("");
                bodyCell.setCellStyle(bodyStyle);
                //reportSheet.autoSizeColumn(col);
            }
        }

        for(int i=0; i<headers.length; i++){
            Cell headerCell = rowHeaders.createCell(i);
            headerCell.setCellValue(headers[i]);
            headerCell.setCellStyle(headerStyle);
            reportSheet.autoSizeColumn(i);
        }


        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        return new ByteArrayInputStream(outputStream.toByteArray());
    }
}
