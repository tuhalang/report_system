package com.viettel.InterconnectSystem.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@PropertySource({"classpath:database.properties"})
@EnableJpaRepositories(
        basePackages = "com.viettel.InterconnectSystem.repository.htds",
        entityManagerFactoryRef = "htdsEntityManagerFactory",
        transactionManagerRef = "htdsTransactionManager"
)
public class PersistenceHtdsConfig {

    public PersistenceHtdsConfig() {
        super();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.htds")
    public DataSourceProperties htdsDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("spring.datasource.htds.configuration")
    public DataSource htdsDataSource() {
        return htdsDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }

    @PersistenceContext(unitName = "htdsPersistence")
    @Bean(name = "htdsEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean htdsEntityManagerFactory(
            EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(htdsDataSource())
                .packages("com.viettel.InterconnectSystem.model.htds")
                .build();
    }

    @Bean(name = "htdsTransactionManager")
    public PlatformTransactionManager htdsTransactionManager(
            final @Qualifier("htdsEntityManagerFactory") LocalContainerEntityManagerFactoryBean htdsEntityManagerFactory) {
        return new JpaTransactionManager(htdsEntityManagerFactory.getObject());
    }
}
