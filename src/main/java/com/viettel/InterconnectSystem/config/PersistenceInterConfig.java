package com.viettel.InterconnectSystem.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@PropertySource({"classpath:database.properties"})
@EnableJpaRepositories(
        basePackages = "com.viettel.InterconnectSystem.repository.inter",
        entityManagerFactoryRef = "interEntityManagerFactory",
        transactionManagerRef = "interTransactionManager"
)
public class PersistenceInterConfig {

    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource.inter")
    public DataSourceProperties interDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource.inter.configuration")
    public DataSource interDataSource() {
        return interDataSourceProperties().initializeDataSourceBuilder()
                .type(HikariDataSource.class).build();
    }

    @Primary
    @PersistenceContext(unitName = "interPersistence")
    @Bean(name = "interEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean interEntityManagerFactory(
            EntityManagerFactoryBuilder builder) {
        return builder
                .dataSource(interDataSource())
                .packages("com.viettel.InterconnectSystem.model.inter")
                .build();
    }

    @Primary
    @Bean(name = "interTransactionManager")
    public PlatformTransactionManager interTransactionManager(
            final @Qualifier("interEntityManagerFactory") LocalContainerEntityManagerFactoryBean mainEntityManagerFactory) {
        return new JpaTransactionManager(mainEntityManagerFactory.getObject());
    }
}
