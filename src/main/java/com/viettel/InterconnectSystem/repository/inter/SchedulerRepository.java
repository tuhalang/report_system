package com.viettel.InterconnectSystem.repository.inter;

import com.viettel.InterconnectSystem.model.inter.Scheduler;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SchedulerRepository extends JpaRepository<Scheduler, Long> {

    Page<Scheduler> findAllByStatus(Pageable pageable, Integer isDelete);

    @Query(
            value = "SELECT s from Scheduler s where s.status=:deleted and s.synthTypeId=:synthTypeId"
    )
    Page<Scheduler> findAllByIsDeleteAndSynthTypeId(Pageable pageable, @Param("deleted") Integer deleted,
                                                  @Param("synthTypeId") Long synthTypeId);

    Scheduler findBySynthTypeId(Long countryId);
}
