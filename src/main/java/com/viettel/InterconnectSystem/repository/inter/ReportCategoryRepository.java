package com.viettel.InterconnectSystem.repository.inter;

import com.viettel.InterconnectSystem.model.inter.ReportCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportCategoryRepository extends JpaRepository<ReportCategory, Long> {

    Page<ReportCategory> findAllByStatus(Pageable pageable, Integer status);

    @Query(
            value = "SELECT r from ReportCategory r where r.status=:status " +
                    "and lower(r.reportName) like lower(concat(:reportName,'%'))"
    )
    Page<ReportCategory> findAllByStatusAndReportName(Pageable pageable, Integer status, String reportName);

    ReportCategory findByReportId(Long reportId);
}
