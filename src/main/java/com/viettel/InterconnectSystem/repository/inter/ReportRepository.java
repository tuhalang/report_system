package com.viettel.InterconnectSystem.repository.inter;

import java.util.Date;
import java.util.List;

public interface ReportRepository {

    List getReportData(String procedureName, Long reportTypeId, Long partnerId, Date date);
}
