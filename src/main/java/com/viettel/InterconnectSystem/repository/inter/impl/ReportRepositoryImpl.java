package com.viettel.InterconnectSystem.repository.inter.impl;

import com.viettel.InterconnectSystem.repository.inter.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Repository
public class ReportRepositoryImpl implements ReportRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List getReportData(String procedureName, Long reportTypeId, Long partnerId, Date date) {
        LOGGER.info("start call procedure: " + procedureName + " with params: (p_date: "+ date + ", p_report_type_id: "+ reportTypeId+", p_partner_id: "+partnerId+")");
        long start = System.currentTimeMillis();
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery(procedureName);
        storedProcedureQuery.registerStoredProcedureParameter("p_date", Date.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("p_report_type_id", Long.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("p_partner_id", Long.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("prc", Class.class, ParameterMode.REF_CURSOR);
        storedProcedureQuery.setParameter("p_date", date);
        storedProcedureQuery.setParameter("p_report_type_id", reportTypeId);
        storedProcedureQuery.setParameter("p_partner_id", partnerId);
        if(storedProcedureQuery.execute()) {
            List results = storedProcedureQuery.getResultList();
            LOGGER.info("Call procedure success: " + results.size() + " records in " + (System.currentTimeMillis() - start) + " ms");
            return results;
        }
        LOGGER.info("Call procedure error in " + (System.currentTimeMillis() - start) + " ms");
        return null;
    }
}
