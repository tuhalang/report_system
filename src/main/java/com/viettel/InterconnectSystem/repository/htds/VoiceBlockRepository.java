package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.VoiceBlock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VoiceBlockRepository extends JpaRepository<VoiceBlock, Long> {


    @Query(
        value = "select v from VoiceBlock v where v.isDelete=:isDelete and lower(v.blockName) like lower(concat('%', :blockName,'%'))"
    )
    Page<VoiceBlock> findByIsDeleteAndBlockName(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("blockName") String blockName);
}
