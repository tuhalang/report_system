package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.GetProcessParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GetProcessParamRepository extends JpaRepository<GetProcessParam, Long> {

    @Query(
        value = "select new com.viettel.InterconnectSystem.controller.bean.GetProcessParamResponse(g, sb.switchName) from GetProcessParam g " +
                "left join SwitchBoard sb on sb.switchId=g.switchId " +
                "where g.isDelete=:isDelete order by g.getId desc"
    )
    Page<GetProcessParam> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);


    @Query(
        value = "select new com.viettel.InterconnectSystem.controller.bean.GetProcessParamResponse(g, sb.switchName) from GetProcessParam g " +
                "left join SwitchBoard sb on sb.switchId=g.switchId " +
                "where g.isDelete=:isDelete and " +
                "lower(g.processCode) like lower(concat('%', :processCode,'%')) order by g.getId desc"
    )
    Page<GetProcessParam>  findAllByIsDeleteAndProcessCode(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("processCode") String processCode);

    GetProcessParam findByGetId(Long getId);
}
