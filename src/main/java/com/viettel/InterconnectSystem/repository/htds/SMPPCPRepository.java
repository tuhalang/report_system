package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.SMPPCP;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SMPPCPRepository extends JpaRepository<SMPPCP, Long> {

    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.SMPPCPResponse(s, p.partName, sb.switchName) from SMPPCP s " +
                    "join Partner p on p.partnerId=s.partnerId join SwitchBoard sb on sb.switchId=s.switchId " +
                    "where s.isDelete=:isDelete order by s.smppCpId desc"
    )
    Page<SMPPCP> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);


    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.SMPPCPResponse(s, p.partName, sb.switchName) from SMPPCP s " +
                    "join Partner p on p.partnerId=s.partnerId join SwitchBoard sb on sb.switchId=s.switchId " +
                    "where s.isDelete=:isDelete and " +
                    "lower(s.smppCpName) like lower(concat('%', :smppCpName,'%'))  order by s.smppCpId desc"
    )
    Page<SMPPCP>  findAllByIsDeleteAndSmppCpName(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("smppCpName") String smppCpName);

    SMPPCP findBySmppCpId(Long SmppCpId);

    SMPPCP findBySmppCpNameAndSwitchId(String smppCpName, Long switchId);
}
