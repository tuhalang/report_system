package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.PreInterNumGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface PreInterNumGroupRepository extends JpaRepository<PreInterNumGroup, Long> {

    @Query(value = "select htds.pre_inter_num_group_seq.nextval from dual", nativeQuery = true)
    BigDecimal getNextSequence();

    @Query(
            value = "select * from pre_inter_num_group where partner_id = :partnerId " +
                    "and number_group_name = :numberGroupName and inter_code_price_id = :interCodePriceId " +
                    "and is_delete = 0 and rownum = 1",
            nativeQuery = true
    )
    PreInterNumGroup findByPartnerIdAndNumberGroupNameAndInterCodePriceId(
            @Param("partnerId") Long partnerId,
            @Param("numberGroupName") String numberGroupName,
            @Param("interCodePriceId") Long interCodePriceId);
}
