package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {

    @Query(
        value = "select new com.viettel.InterconnectSystem.controller.bean.ServiceResponse(s, v.blockCode, sw.switchName, p.partName) from Service s " +
                "left join VoiceBlock v on s.voiceBlockId = v.voiceBlockId left join Partner p on s.partnerId = p.partnerId " +
                "left join SwitchBoard sw on s.switchId = sw.switchId where s.isDelete = :isDelete order by s.serviceId desc"
    )
    Page<Service> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);


    @Query(
        value = "select new com.viettel.InterconnectSystem.controller.bean.ServiceResponse(s, v.blockCode, sw.switchName, p.partName) from Service s " +
                "left join VoiceBlock v on s.voiceBlockId = v.voiceBlockId left join Partner p on s.partnerId = p.partnerId " +
                "left join SwitchBoard sw on s.switchId = sw.switchId where s.isDelete = :isDelete and" +
                " lower(s.serviceName) like lower(concat('%', :serviceName,'%')) order by s.serviceId desc"
    )
    Page<Service>  findAllByIsDeleteAndServiceName(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("serviceName") String serviceName);

    Service findByServiceId(Long serviceId);
}
