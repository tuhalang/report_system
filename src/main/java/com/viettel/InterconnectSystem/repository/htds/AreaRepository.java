package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.Area;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaRepository extends JpaRepository<Area, Long> {

    @Query(
            value = "select a from Area a where a.isDelete=:isDelete and lower(a.name) like lower(concat('%', :areaName,'%'))"
    )
    Page<Area> findByIsDeleteAndName(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("areaName") String areaName);
}
