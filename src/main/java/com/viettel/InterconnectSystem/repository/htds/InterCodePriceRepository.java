package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.InterCodePrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InterCodePriceRepository extends JpaRepository<InterCodePrice, Long> {


    @Query(
            value = "select * from INTER_CODE_PRICE a where a.PARTNER_ID = :partnerId and INTER_TYPE = :interType and a.IS_DELETE = 0 and rownum = 1 order by DATE_CREATE desc",
            nativeQuery = true
    )
    InterCodePrice findByPartnerIdAndInterType(@Param("partnerId") Long partnerId,
                                               @Param("interType") String interType);
}
