package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.SwitchBoard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SwitchBoardRepository extends JpaRepository<SwitchBoard, Long> {

    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.SwitchBoardResponse(s, a.name) from SwitchBoard s " +
                    "join Area a on a.areaId=s.areaId where s.isDelete = :isDelete order by s.switchId desc"
    )
    Page<SwitchBoard> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);


    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.SwitchBoardResponse(s, a.name) from SwitchBoard s " +
                    "join Area a on a.areaId=s.areaId where s.isDelete = :isDelete and  lower(s.switchName) like lower(concat('%', :switchName,'%')) order by s.switchId desc"
    )
    Page<SwitchBoard>  findAllByIsDeleteAndSwitchName(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("switchName") String switchName);

    SwitchBoard findBySwitchId(Long switchId);
}
