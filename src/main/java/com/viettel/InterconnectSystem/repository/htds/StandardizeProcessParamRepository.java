package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.StandardizeProcessParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StandardizeProcessParamRepository extends JpaRepository<StandardizeProcessParam, Long> {

    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.StandardizeProcessParamResponse(s, sb.switchName) from StandardizeProcessParam s " +
                    "join SwitchBoard sb on sb.switchId=s.switchId " +
                    "where s.isDelete=:isDelete order by s.standardizeId desc"
    )
    Page<StandardizeProcessParam> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);


    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.StandardizeProcessParamResponse(s, sb.switchName) from StandardizeProcessParam s " +
                    "join SwitchBoard sb on sb.switchId=s.switchId " +
                    "where s.isDelete=:isDelete and " +
                    "lower(s.processCode) like lower(concat('%', :processCode,'%')) order by s.standardizeId desc"
    )
    Page<StandardizeProcessParam>  findAllByIsDeleteAndProcessCode(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("processCode") String processCode);

    StandardizeProcessParam findByStandardizeId(Long standardizeId);
}
