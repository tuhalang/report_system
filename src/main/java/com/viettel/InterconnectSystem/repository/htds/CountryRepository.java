package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface CountryRepository extends PagingAndSortingRepository<Country, Long> {

    @Query(
            value = "SELECT c FROM Country c WHERE c.isDelete=:isDelete order by c.countryId desc"
    )
    Page<Country> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);

    @Query(
            value = "SELECT c from Country c where c.isDelete=:deleted and lower(c.countryName) " +
                    "like lower(concat('%', :countryName,'%')) order by c.countryId desc"
    )
    Page<Country> findAllByIsDeleteAndCountryName(Pageable pageable, @Param("deleted") Integer deleted,
                                                  @Param("countryName") String countryName);

    Country findByCountryId(Long countryId);

}
