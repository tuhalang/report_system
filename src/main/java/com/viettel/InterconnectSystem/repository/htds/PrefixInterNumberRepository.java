package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.PrefixInterNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface PrefixInterNumberRepository extends JpaRepository<PrefixInterNumber, Long> {

    @Query(value = "select htds.prefix_inter_number_seq.nextval from dual", nativeQuery = true)
    BigDecimal getNextSequence();

    @Query(
        value = "select * from prefix_inter_number where pre_inter_num_group_id = :preInterNumGroupId and prefix_number = :prefixNumber and is_delete = 0 and rownum = 1",
        nativeQuery = true
    )
    PrefixInterNumber findByPreInterNumGroupIdAndPrefixNumber(@Param("preInterNumGroupId") BigDecimal preInterNumGroupId, @Param("prefixNumber") String prefixNumber);
}
