package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.view.FullPreInterNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FullPreInterNumberRepository extends JpaRepository<FullPreInterNumber, Long> {

    List<FullPreInterNumber> findAllByPartnerIdAndInterTypeAndDirection(
            Long partnerId, String interType, String direction);
}
