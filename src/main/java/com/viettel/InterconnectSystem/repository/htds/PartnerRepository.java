package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.Partner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PartnerRepository extends PagingAndSortingRepository<Partner, Long> {

    @Query(
        value = "SELECT new com.viettel.InterconnectSystem.controller.bean.PartnerResponse(" +
                "p.partnerId, p.countryId, p.address, p.phone, p.fax, p.partName, p.partAbbreviate," +
                "p.isDelete, p.isNational, p.isVas, p.timezoneOutDiff, p.timezoneInDiff, c.countryName" +
                ") " +
                "from Partner p inner join Country c on p.countryId=c.countryId where p.isDelete=:deleted order by p.partnerId desc"
    )
    Page<Partner> findAllByIsDelete(Pageable pageable, @Param("deleted") Integer deleted);

    @Query(
            value = "SELECT new com.viettel.InterconnectSystem.controller.bean.PartnerResponse(" +
                    "p.partnerId, p.countryId, p.address, p.phone, p.fax, p.partName, p.partAbbreviate," +
                    "p.isDelete, p.isNational, p.isVas, p.timezoneOutDiff, p.timezoneInDiff, c.countryName" +
                    ") " +
                    "from Partner p inner join Country c on p.countryId=c.countryId where p.isDelete=:deleted" +
                    " and lower(p.partName) like lower(concat('%', :partName,'%')) order by p.partnerId desc"
    )
    Page<Partner> findAllByIsDeleteAndPartName(Pageable pageable, @Param("deleted") Integer deleted,
                                               @Param("partName") String partName);

    Partner findByPartnerId(Long partnerId);
}
