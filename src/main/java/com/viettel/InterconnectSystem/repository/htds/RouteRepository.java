package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.Route;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {

    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.RouteResponse(r, p.partName, sb.switchName, pn.prefixNumber, s.serviceName) from Route r " +
                    "left join Partner p on p.partnerId=r.partnerId left join SwitchBoard sb on sb.switchId=r.switchId left join PrefixNumber pn on pn.prefixNumberId=r.prefixNumberId " +
                    "left join Service s on s.serviceId = r.serviceId where r.isDelete=:isDelete order by r.routeId desc"
    )
    Page<Route> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);


    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.RouteResponse(r, p.partName, sb.switchName, pn.prefixNumber, s.serviceName) from Route r " +
                    "left join Partner p on p.partnerId=r.partnerId left join SwitchBoard sb on sb.switchId=r.switchId left join PrefixNumber pn on pn.prefixNumberId=r.prefixNumberId " +
                    "left join Service s on s.serviceId = r.serviceId where r.isDelete=:isDelete and " +
                    "lower(r.routeName) like lower(concat('%', :routeName,'%')) order by r.routeId desc"
    )
    Page<Route>  findAllByIsDeleteAndRouteName(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("routeName") String routeName);

    Route findByRouteId(Long serviceId);

    Route findByRouteNameAndServiceId(String routeName, Long switchId);
}
