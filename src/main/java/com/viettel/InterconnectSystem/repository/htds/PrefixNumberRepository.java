package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.PrefixNumber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PrefixNumberRepository extends JpaRepository<PrefixNumber, Long> {

    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.PrefixNumberResponse(p, c.countryName, s.serviceName, a.name, pn.partName) from PrefixNumber p " +
                    "left join Country c on c.countryId=p.countryId left join Service s on s.serviceId = p.serviceId left join Area a on a.areaId=p.areaId " +
                    "left join Partner pn on p.partnerId = pn.partnerId " +
                    "where p.isDelete=:isDelete order by p.prefixNumberId desc"
    )
    Page<PrefixNumber> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);


    @Query(
            value = "select new com.viettel.InterconnectSystem.controller.bean.PrefixNumberResponse(p, c.countryName, s.serviceName, a.name, pn.partName) from PrefixNumber p " +
                    "left join Country c on c.countryId=p.countryId left join Service s on s.serviceId = p.serviceId left join Area a on a.areaId=p.areaId " +
                    "left join Partner pn on p.partnerId = pn.partnerId " +
                    "where p.isDelete=:isDelete and " +
                    "lower(p.prefixNumber) like lower(concat('%', :prefixNumber,'%')) order by p.prefixNumberId desc"
    )
    Page<PrefixNumber>  findAllByIsDeleteAndPrefixName(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("prefixNumber") String prefixNumber);

    PrefixNumber findByPrefixNumberId(Long prefixNumberId);
}
