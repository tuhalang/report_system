package com.viettel.InterconnectSystem.repository.htds;

import com.viettel.InterconnectSystem.model.htds.SmsCenter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsCenterRepository extends JpaRepository<SmsCenter, Long> {

    @Query(
        value = "select new com.viettel.InterconnectSystem.controller.bean.SmsCenterResponse(s, p.partName) from SmsCenter s " +
                "join Partner p on p.partnerId=s.partnerId " +
                "where s.isDelete=:isDelete order by s.smsCenterId desc"
    )
    Page<SmsCenter> findAllByIsDelete(Pageable pageable, @Param("isDelete") Integer isDelete);


    @Query(
        value = "select new com.viettel.InterconnectSystem.controller.bean.SmsCenterResponse(s, p.partName) from SmsCenter s " +
                "join Partner p on p.partnerId=s.partnerId " +
                "where s.isDelete=:isDelete and " +
                "lower(s.partType) like lower(concat('%', :partType,'%')) order by s.smsCenterId desc"
    )
    Page<SmsCenter>  findAllByIsDeleteAndPartType(Pageable pageable, @Param("isDelete") Integer isDelete, @Param("partType") String partType);

    SmsCenter findBySmsCenterId(Long SmppCpId);
}
