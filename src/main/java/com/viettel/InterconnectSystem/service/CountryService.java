package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Country;
import org.springframework.data.domain.Pageable;

public interface CountryService {

    void searchByCountryName(Pageable pageable, ResponseBean responseBean, String key, Integer deleted) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(Country country, ResponseBean responseBean) throws Exception;
    void create(Country country, ResponseBean responseBean) throws Exception;
    void delete(Long countryId, Integer deleted, ResponseBean responseBean) throws Exception;
}
