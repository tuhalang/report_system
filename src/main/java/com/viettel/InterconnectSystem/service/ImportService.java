package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.FileUpload;

import java.io.IOException;

public interface ImportService {

    void importPrice(FileUpload fileUpload) throws IOException, Exception;
}
