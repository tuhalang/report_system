package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SwitchBoard;
import org.springframework.data.domain.Pageable;

public interface SwitchBoardService {
    void searchByIsDeleteAndSwitchName(Pageable pageable, ResponseBean responseBean, Integer deleted, String switchName) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(SwitchBoard switchBoard, ResponseBean responseBean) throws Exception;
    void create(SwitchBoard switchBoard, ResponseBean responseBean) throws Exception;
    void delete(Long switchBoardId, Integer deleted, ResponseBean responseBean) throws Exception;
}
