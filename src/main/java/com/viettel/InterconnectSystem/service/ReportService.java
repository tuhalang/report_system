package com.viettel.InterconnectSystem.service;

import java.io.ByteArrayInputStream;
import java.util.Date;

public interface ReportService {

    ByteArrayInputStream getReportData(Long categoryId, Long partnerId, Date date);

    ByteArrayInputStream getInterPrice(Long partnerId, String interType, String direction);
}
