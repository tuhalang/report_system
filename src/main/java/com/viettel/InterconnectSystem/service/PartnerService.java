package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Partner;
import org.springframework.data.domain.Pageable;

public interface PartnerService {

    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void searchByPartName(Pageable pageable, ResponseBean responseBean, String key, Integer deleted) throws Exception;
    void update(Partner partner, ResponseBean responseBean) throws Exception;
    void create(Partner partner, ResponseBean responseBean) throws Exception;
    void delete(Long partnerId, Integer deleted, ResponseBean responseBean) throws Exception;
}
