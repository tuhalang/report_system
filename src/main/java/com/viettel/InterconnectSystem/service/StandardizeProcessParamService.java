package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.StandardizeProcessParam;
import org.springframework.data.domain.Pageable;

public interface StandardizeProcessParamService {
    void searchByIsDeleteAndProcessCode(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(StandardizeProcessParam standardizeProcessParam, ResponseBean responseBean) throws Exception;
    void create(StandardizeProcessParam standardizeProcessParam, ResponseBean responseBean) throws Exception;
    void delete(Long standardizeId, Integer deleted, ResponseBean responseBean) throws Exception;
}
