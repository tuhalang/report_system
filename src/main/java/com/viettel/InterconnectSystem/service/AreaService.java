package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import org.springframework.data.domain.Pageable;

public interface AreaService {
    void searchByIsDeleteAndName(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception;
}
