package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Route;
import org.springframework.data.domain.Pageable;

public interface RouteService {
    void searchByIsDeleteAndRouteName(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(Route route, ResponseBean responseBean) throws Exception;
    void create(Route route, ResponseBean responseBean) throws Exception;
    void delete(Long routeId, Integer deleted, ResponseBean responseBean) throws Exception;
}
