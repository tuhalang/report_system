package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.inter.Scheduler;
import org.springframework.data.domain.Pageable;

public interface SchedulerService {

    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void searchBySynthTypeId(Pageable pageable, ResponseBean responseBean, Long key, Integer deleted) throws Exception;
    void update(Scheduler scheduler, ResponseBean responseBean) throws Exception;
    void create(Scheduler scheduler, ResponseBean responseBean) throws Exception;
    void delete(Long synthTypeId, Integer deleted, ResponseBean responseBean) throws Exception;
}
