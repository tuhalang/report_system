package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Service;
import org.springframework.data.domain.Pageable;

public interface ServiceService {
    void searchByServiceName(Pageable pageable, ResponseBean responseBean, Integer deleted, String serviceName) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(Service service, ResponseBean responseBean) throws Exception;
    void create(Service service, ResponseBean responseBean) throws Exception;
    void delete(Long serviceId, Integer deleted, ResponseBean responseBean) throws Exception;
}
