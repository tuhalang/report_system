package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.GetProcessParam;
import org.springframework.data.domain.Pageable;

public interface GetProcessParamService {
    void searchByIsDeleteAndProcessCode(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(GetProcessParam getProcessParam, ResponseBean responseBean) throws Exception;
    void create(GetProcessParam getProcessParam, ResponseBean responseBean) throws Exception;
    void delete(Long getId, Integer deleted, ResponseBean responseBean) throws Exception;
}
