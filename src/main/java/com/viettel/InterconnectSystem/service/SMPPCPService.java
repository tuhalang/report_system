package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SMPPCP;
import org.springframework.data.domain.Pageable;

public interface SMPPCPService {
    void searchByIsDeleteAndSmppCpName(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(SMPPCP smppCp, ResponseBean responseBean) throws Exception;
    void create(SMPPCP smppCp, ResponseBean responseBean) throws Exception;
    void delete(Long smppCpId, Integer deleted, ResponseBean responseBean) throws Exception;
}
