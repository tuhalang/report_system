package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.ConvertProcessParam;
import org.springframework.data.domain.Pageable;

public interface ConvertProcessParamService {
    void searchByIsDeleteAndProcessCode(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(ConvertProcessParam convertProcessParam, ResponseBean responseBean) throws Exception;
    void create(ConvertProcessParam convertProcessParam, ResponseBean responseBean) throws Exception;
    void delete(Long convertProcessParamId, Integer deleted, ResponseBean responseBean) throws Exception;
}
