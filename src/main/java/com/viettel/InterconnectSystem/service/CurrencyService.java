package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.model.htds.Currency;

import java.util.List;

public interface CurrencyService {

    List<Currency> findAll();
}
