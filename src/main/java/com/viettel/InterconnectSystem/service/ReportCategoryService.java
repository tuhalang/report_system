package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.inter.ReportCategory;
import org.springframework.data.domain.Pageable;

public interface ReportCategoryService {
    void searchByReportName(Pageable pageable, ResponseBean responseBean, String key, Integer deleted) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(ReportCategory reportCategory, ResponseBean responseBean) throws Exception;
    void create(ReportCategory reportCategory, ResponseBean responseBean) throws Exception;
    void delete(Long reportCategoryId, Integer deleted, ResponseBean responseBean) throws Exception;
}
