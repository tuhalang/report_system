package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.PrefixNumber;
import org.springframework.data.domain.Pageable;

public interface PrefixNumberService {
    void searchByIsDeleteAndPrefixNumber(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(PrefixNumber prefixNumber, ResponseBean responseBean) throws Exception;
    void create(PrefixNumber prefixNumber, ResponseBean responseBean) throws Exception;
    void delete(Long prefixNumberId, Integer deleted, ResponseBean responseBean) throws Exception;
}
