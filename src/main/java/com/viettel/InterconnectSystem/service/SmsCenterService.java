package com.viettel.InterconnectSystem.service;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SmsCenter;
import org.springframework.data.domain.Pageable;

public interface SmsCenterService {
    void searchByIsDeleteAndPartType(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception;
    void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception;
    void update(SmsCenter smsCenter, ResponseBean responseBean) throws Exception;
    void create(SmsCenter smsCenter, ResponseBean responseBean) throws Exception;
    void delete(Long smsCenterId, Integer deleted, ResponseBean responseBean) throws Exception;
}
