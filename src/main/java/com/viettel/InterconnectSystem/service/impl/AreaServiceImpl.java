package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Area;
import com.viettel.InterconnectSystem.repository.htds.AreaRepository;
import com.viettel.InterconnectSystem.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class AreaServiceImpl implements AreaService {

    private static final int DELETED = 1;

    @Autowired
    private AreaRepository areaRepository;

    @Override
    public void searchByIsDeleteAndName(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception {
        Page<Area> areas = null;
        if(deleted == DELETED){
            areas = areaRepository.findByIsDeleteAndName(pageable, 1, key);
        }else{
            areas = areaRepository.findByIsDeleteAndName(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", areas.getTotalElements());
        wsResponse.put("items", areas.getContent());
        responseBean.setData(wsResponse);
    }
}
