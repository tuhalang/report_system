package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.PrefixNumber;
import com.viettel.InterconnectSystem.repository.htds.PrefixNumberRepository;
import com.viettel.InterconnectSystem.service.PrefixNumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class PrefixNumberServiceImpl implements PrefixNumberService {

    private static final int DELETED = 1;

    @Autowired
    private PrefixNumberRepository prefixNumberRepository;

    @Override
    public void searchByIsDeleteAndPrefixNumber(Pageable pageable, ResponseBean responseBean,
                                                Integer deleted, String key) throws Exception {
        Page<PrefixNumber> prefixNumbers = null;
        if(deleted == DELETED){
            prefixNumbers = prefixNumberRepository.findAllByIsDeleteAndPrefixName(pageable, 1, key);
        }else{
            prefixNumbers = prefixNumberRepository.findAllByIsDeleteAndPrefixName(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", prefixNumbers.getTotalElements());
        wsResponse.put("items", prefixNumbers.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<PrefixNumber> prefixNumbers = null;
        if(deleted == DELETED){
            prefixNumbers = prefixNumberRepository.findAllByIsDelete(pageable, 1);
        }else{
            prefixNumbers = prefixNumberRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", prefixNumbers.getTotalElements());
        wsResponse.put("items", prefixNumbers.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(PrefixNumber prefixNumber, ResponseBean responseBean) throws Exception {
        PrefixNumber oldPrefixNumber = prefixNumberRepository.findByPrefixNumberId(prefixNumber.getPrefixNumberId());
        if(oldPrefixNumber == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        prefixNumberRepository.save(prefixNumber);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }

    @Override
    public void create(PrefixNumber prefixNumber, ResponseBean responseBean) throws Exception {
        prefixNumber.setIsDelete(0);
        prefixNumber.setPrefixNumberId(null);
        prefixNumberRepository.save(prefixNumber);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long prefixNumberId, Integer deleted, ResponseBean responseBean) throws Exception {
        PrefixNumber prefixNumber = prefixNumberRepository.findByPrefixNumberId(prefixNumberId);
        if(prefixNumber == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        prefixNumber.setIsDelete(deleted);
        prefixNumberRepository.save(prefixNumber);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
