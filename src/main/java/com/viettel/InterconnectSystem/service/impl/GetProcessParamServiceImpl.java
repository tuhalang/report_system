package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.GetProcessParam;
import com.viettel.InterconnectSystem.repository.htds.GetProcessParamRepository;
import com.viettel.InterconnectSystem.service.GetProcessParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class GetProcessParamServiceImpl implements GetProcessParamService {

    private static final int DELETED = 1;

    @Autowired
    private GetProcessParamRepository getProcessParamRepository;


    @Override
    public void searchByIsDeleteAndProcessCode(Pageable pageable, ResponseBean responseBean,
                                               Integer deleted, String key) throws Exception{
        Page<GetProcessParam> getProcessParams = null;
        if(deleted == DELETED){
            getProcessParams = getProcessParamRepository.findAllByIsDeleteAndProcessCode(pageable, 1, key);
        }else{
            getProcessParams = getProcessParamRepository.findAllByIsDeleteAndProcessCode(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", getProcessParams.getTotalElements());
        wsResponse.put("items", getProcessParams.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<GetProcessParam> getProcessParams = null;
        if(deleted == DELETED){
            getProcessParams = getProcessParamRepository.findAllByIsDelete(pageable, 1);
        }else{
            getProcessParams = getProcessParamRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", getProcessParams.getTotalElements());
        wsResponse.put("items", getProcessParams.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(GetProcessParam getProcessParam, ResponseBean responseBean) throws Exception {
        GetProcessParam oldProcessParam = getProcessParamRepository.findByGetId(getProcessParam.getGetId());
        if(oldProcessParam == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        getProcessParamRepository.save(getProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(GetProcessParam getProcessParam, ResponseBean responseBean) throws Exception {
        getProcessParam.setIsDelete(0);
        getProcessParam.setGetId(null);
        getProcessParamRepository.save(getProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long getId, Integer deleted, ResponseBean responseBean) throws Exception {
        GetProcessParam getProcessParam = getProcessParamRepository.findByGetId(getId);
        if(getProcessParam == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        getProcessParam.setIsDelete(deleted);
        getProcessParamRepository.save(getProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
