package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.repository.htds.ServiceRepository;
import com.viettel.InterconnectSystem.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class ServiceServiceImpl implements ServiceService {

    private static final int DELETED = 1;

    @Autowired
    private ServiceRepository serviceRepository;


    @Override
    public void searchByServiceName(Pageable pageable, ResponseBean responseBean,
                                    Integer deleted, String key) throws Exception {
        Page<com.viettel.InterconnectSystem.model.htds.Service> reportCategories = null;
        if(deleted == DELETED){
            reportCategories = serviceRepository.findAllByIsDeleteAndServiceName(pageable, 1, key);
        }else{
            reportCategories = serviceRepository.findAllByIsDeleteAndServiceName(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", reportCategories.getTotalElements());
        wsResponse.put("items", reportCategories.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<com.viettel.InterconnectSystem.model.htds.Service> services = null;
        if(deleted == DELETED){
            services = serviceRepository.findAllByIsDelete(pageable, 1);
        }else{
            services = serviceRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", services.getTotalElements());
        wsResponse.put("items", services.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(com.viettel.InterconnectSystem.model.htds.Service service, ResponseBean responseBean) throws Exception {
        com.viettel.InterconnectSystem.model.htds.Service oldService = serviceRepository.findByServiceId(service.getServiceId());
        if(oldService == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        oldService.setBlockType(service.getBlockType());
        oldService.setIsDelete(service.getIsDelete());
        oldService.setPartnerId(service.getPartnerId());
        oldService.setServiceCode(service.getServiceCode());
        oldService.setServiceName(service.getServiceName());
        oldService.setServiceType(service.getServiceType());
        oldService.setSwitchId(service.getSwitchId());
        oldService.setTimeOffPeakId(service.getTimeOffPeakId());
        oldService.setVoiceBlockId(service.getVoiceBlockId());

        serviceRepository.save(oldService);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(com.viettel.InterconnectSystem.model.htds.Service service, ResponseBean responseBean) throws Exception {
        service.setIsDelete(0);
        service.setServiceId(null);
        serviceRepository.save(service);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long serviceId, Integer deleted, ResponseBean responseBean) throws Exception {
        com.viettel.InterconnectSystem.model.htds.Service service = serviceRepository.findByServiceId(serviceId);
        if(service == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        service.setIsDelete(deleted);
        serviceRepository.save(service);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
