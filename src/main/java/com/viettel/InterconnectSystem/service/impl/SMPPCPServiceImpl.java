package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SMPPCP;
import com.viettel.InterconnectSystem.repository.htds.SMPPCPRepository;
import com.viettel.InterconnectSystem.service.SMPPCPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class SMPPCPServiceImpl implements SMPPCPService {

    @Autowired
    private SMPPCPRepository smppcpRepository;

    private static final int DELETED = 1;

    @Override
    public void searchByIsDeleteAndSmppCpName(Pageable pageable, ResponseBean responseBean,
                                              Integer deleted, String key) throws Exception{
        Page<SMPPCP> smppcps = null;
        if(deleted == DELETED){
            smppcps = smppcpRepository.findAllByIsDeleteAndSmppCpName(pageable, 1, key);
        }else{
            smppcps = smppcpRepository.findAllByIsDeleteAndSmppCpName(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", smppcps.getTotalElements());
        wsResponse.put("items", smppcps.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<SMPPCP> smppcps = null;
        if(deleted == DELETED){
            smppcps = smppcpRepository.findAllByIsDelete(pageable, 1);
        }else{
            smppcps = smppcpRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", smppcps.getTotalElements());
        wsResponse.put("items", smppcps.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(SMPPCP smppCp, ResponseBean responseBean) throws Exception {
        SMPPCP oldRoute = smppcpRepository.findBySmppCpId(smppCp.getSmppCpId());
        if(oldRoute == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("SmppCp not found !");
            return;
        }
        SMPPCP s = smppcpRepository.findBySmppCpNameAndSwitchId(smppCp.getSmppCpName(), smppCp.getSwitchId());
        if(oldRoute.getSwitchId() != smppCp.getSwitchId() && s != null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("SmppCp name and switch are exists !");
            return;
        }
        smppcpRepository.save(smppCp);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(SMPPCP smppCp, ResponseBean responseBean) throws Exception {
        SMPPCP s = smppcpRepository.findBySmppCpNameAndSwitchId(smppCp.getSmppCpName(), smppCp.getSwitchId());
        if(s != null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("SmppCp name and switch are exists !");
            return;
        }
        smppCp.setIsDelete(0);
        smppCp.setSmppCpId(null);
        smppcpRepository.save(smppCp);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long smppCpId, Integer deleted, ResponseBean responseBean) throws Exception {
        SMPPCP smppcp = smppcpRepository.findBySmppCpId(smppCpId);
        if(smppcp == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        smppcp.setIsDelete(deleted);
        smppcpRepository.save(smppcp);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
