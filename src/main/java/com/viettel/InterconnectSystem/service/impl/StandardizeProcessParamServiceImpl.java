package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.StandardizeProcessParam;
import com.viettel.InterconnectSystem.repository.htds.StandardizeProcessParamRepository;
import com.viettel.InterconnectSystem.service.StandardizeProcessParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class StandardizeProcessParamServiceImpl implements StandardizeProcessParamService {

    private static final int DELETED = 1;

    @Autowired
    private StandardizeProcessParamRepository standardizeProcessParamRepository;


    @Override
    public void searchByIsDeleteAndProcessCode(Pageable pageable, ResponseBean responseBean,
                                               Integer deleted, String key) throws Exception {
        Page<StandardizeProcessParam> standardizeProcessParams = null;
        if(deleted == DELETED){
            standardizeProcessParams = standardizeProcessParamRepository.findAllByIsDeleteAndProcessCode(pageable, 1, key);
        }else{
            standardizeProcessParams = standardizeProcessParamRepository.findAllByIsDeleteAndProcessCode(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", standardizeProcessParams.getTotalElements());
        wsResponse.put("items", standardizeProcessParams.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<StandardizeProcessParam> standardizeProcessParams = null;
        if(deleted == DELETED){
            standardizeProcessParams = standardizeProcessParamRepository.findAllByIsDelete(pageable, 1);
        }else{
            standardizeProcessParams = standardizeProcessParamRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", standardizeProcessParams.getTotalElements());
        wsResponse.put("items", standardizeProcessParams.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(StandardizeProcessParam standardizeProcessParam, ResponseBean responseBean) throws Exception {
        StandardizeProcessParam oldStandardizeProcessParam = standardizeProcessParamRepository.findByStandardizeId(standardizeProcessParam.getStandardizeId());
        if(oldStandardizeProcessParam == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        standardizeProcessParamRepository.save(standardizeProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(StandardizeProcessParam standardizeProcessParam, ResponseBean responseBean) throws Exception {
        standardizeProcessParam.setIsDelete(0);
        standardizeProcessParam.setStandardizeId(null);
        standardizeProcessParamRepository.save(standardizeProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long standardizeId, Integer deleted, ResponseBean responseBean) throws Exception {
        StandardizeProcessParam standardizeProcessParam = standardizeProcessParamRepository.findByStandardizeId(standardizeId);
        if(standardizeProcessParam == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        standardizeProcessParam.setIsDelete(deleted);
        standardizeProcessParamRepository.save(standardizeProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
