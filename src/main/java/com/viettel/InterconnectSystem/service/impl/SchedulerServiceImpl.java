package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.inter.Scheduler;
import com.viettel.InterconnectSystem.repository.inter.SchedulerRepository;
import com.viettel.InterconnectSystem.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class SchedulerServiceImpl implements SchedulerService {

    private static final int DELETED = 1;

    @Autowired
    private SchedulerRepository schedulerRepository;


    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<Scheduler> schedulers = null;
        if(deleted == DELETED){
            schedulers = schedulerRepository.findAllByStatus(pageable, 1);
        }else{
            schedulers = schedulerRepository.findAllByStatus(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", schedulers.getTotalElements());
        wsResponse.put("items", schedulers.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void searchBySynthTypeId(Pageable pageable, ResponseBean responseBean, Long key, Integer deleted) throws Exception {
        Page<Scheduler> schedulers = null;
        if(deleted == DELETED){
            schedulers = schedulerRepository.findAllByIsDeleteAndSynthTypeId(pageable, 0, key);
        }else{
            schedulers = schedulerRepository.findAllByIsDeleteAndSynthTypeId(pageable, 1, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", schedulers.getTotalElements());
        wsResponse.put("items", schedulers.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(Scheduler scheduler, ResponseBean responseBean) throws Exception {
        Scheduler oldScheduler = schedulerRepository.findBySynthTypeId(scheduler.getSynthTypeId());
        if(oldScheduler == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Scheduler not found !");
            return;
        }
        schedulerRepository.save(scheduler);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(Scheduler scheduler, ResponseBean responseBean) throws Exception {
        scheduler.setStatus(1);
        schedulerRepository.save(scheduler);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long synthTypeId, Integer deleted, ResponseBean responseBean) throws Exception {
        Scheduler scheduler = schedulerRepository.findBySynthTypeId(synthTypeId);
        if(scheduler == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        scheduler.setStatus(deleted);
        schedulerRepository.save(scheduler);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
