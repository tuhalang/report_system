package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.inter.ReportCategory;
import com.viettel.InterconnectSystem.repository.inter.ReportCategoryRepository;
import com.viettel.InterconnectSystem.service.ReportCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class ReportCategoryServiceImpl implements ReportCategoryService {

    private static final int DELETED = 1;

    @Autowired
    private ReportCategoryRepository reportCategoryRepository;

    @Override
    public void searchByReportName(Pageable pageable, ResponseBean responseBean, String key, Integer deleted) throws Exception{
        Page<ReportCategory> reportCategories = null;
        if(deleted == DELETED){
            reportCategories = reportCategoryRepository.findAllByStatusAndReportName(pageable, 0, key);
        }else{
            reportCategories = reportCategoryRepository.findAllByStatusAndReportName(pageable, 1, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", reportCategories.getTotalElements());
        wsResponse.put("items", reportCategories.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<ReportCategory> reportCategories = null;
        if(deleted == DELETED){
            reportCategories = reportCategoryRepository.findAllByStatus(pageable, 0);
        }else{
            reportCategories = reportCategoryRepository.findAllByStatus(pageable, 1);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", reportCategories.getTotalElements());
        wsResponse.put("items", reportCategories.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(ReportCategory reportCategory, ResponseBean responseBean) throws Exception {
        ReportCategory oldReportCategory = reportCategoryRepository.findByReportId(reportCategory.getReportId());
        if(oldReportCategory == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        oldReportCategory.setReportName(reportCategory.getReportName());
        oldReportCategory.setStatus(reportCategory.getStatus());
        oldReportCategory.setDescription(reportCategory.getDescription());
        oldReportCategory.setExcutePathScript(reportCategory.getExcutePathScript());
        reportCategoryRepository.save(oldReportCategory);

        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(ReportCategory reportCategory, ResponseBean responseBean) throws Exception {
        reportCategory.setStatus(0);
        reportCategory.setReportId(null);
        reportCategoryRepository.save(reportCategory);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long reportCategoryId, Integer deleted, ResponseBean responseBean) throws Exception {
        ReportCategory reportCategory = reportCategoryRepository.findByReportId(reportCategoryId);
        if(reportCategory == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("ReportCategory not found !");
            return;
        }
        reportCategory.setStatus(deleted);
        reportCategoryRepository.save(reportCategory);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
