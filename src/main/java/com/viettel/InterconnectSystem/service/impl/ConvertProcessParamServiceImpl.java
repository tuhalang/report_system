package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.ConvertProcessParam;
import com.viettel.InterconnectSystem.repository.htds.ConvertProcessParamRepository;
import com.viettel.InterconnectSystem.service.ConvertProcessParamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class ConvertProcessParamServiceImpl implements ConvertProcessParamService {

    private static final int DELETED = 1;

    @Autowired
    private ConvertProcessParamRepository convertProcessParamRepository;

    @Override
    public void searchByIsDeleteAndProcessCode(Pageable pageable, ResponseBean responseBean,
                                               Integer deleted, String key) throws Exception{
        Page<ConvertProcessParam> convertProcessParams = null;
        if(deleted == DELETED){
            convertProcessParams = convertProcessParamRepository.findAllByIsDeleteAndProcessCode(pageable, 1, key);
        }else{
            convertProcessParams = convertProcessParamRepository.findAllByIsDeleteAndProcessCode(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", convertProcessParams.getTotalElements());
        wsResponse.put("items", convertProcessParams.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<ConvertProcessParam> convertProcessParams = null;
        if(deleted == DELETED){
            convertProcessParams = convertProcessParamRepository.findAllByIsDelete(pageable, 1);
        }else{
            convertProcessParams = convertProcessParamRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", convertProcessParams.getTotalElements());
        wsResponse.put("items", convertProcessParams.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(ConvertProcessParam convertProcessParam, ResponseBean responseBean) throws Exception {
        ConvertProcessParam oldProcessParam = convertProcessParamRepository.findByConvertId(convertProcessParam.getConvertId());
        if(oldProcessParam == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        convertProcessParamRepository.save(convertProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(ConvertProcessParam convertProcessParam, ResponseBean responseBean) throws Exception {
        convertProcessParam.setIsDelete(0);
        convertProcessParam.setConvertId(null);
        convertProcessParamRepository.save(convertProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long convertProcessParamId, Integer deleted, ResponseBean responseBean) throws Exception {
        ConvertProcessParam convertProcessParam = convertProcessParamRepository.findByConvertId(convertProcessParamId);
        if(convertProcessParam == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        convertProcessParam.setIsDelete(deleted);
        convertProcessParamRepository.save(convertProcessParam);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
