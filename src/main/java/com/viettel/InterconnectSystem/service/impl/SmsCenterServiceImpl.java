package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SmsCenter;
import com.viettel.InterconnectSystem.repository.htds.SmsCenterRepository;
import com.viettel.InterconnectSystem.service.SmsCenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class SmsCenterServiceImpl implements SmsCenterService {

    private static final int DELETED = 1;

    @Autowired
    private SmsCenterRepository smsCenterRepository;

    @Override
    public void searchByIsDeleteAndPartType(Pageable pageable, ResponseBean responseBean,
                                            Integer deleted, String key) throws Exception{
        Page<SmsCenter> smsCenters = null;
        if(deleted == DELETED){
            smsCenters = smsCenterRepository.findAllByIsDeleteAndPartType(pageable, 1, key);
        }else{
            smsCenters = smsCenterRepository.findAllByIsDeleteAndPartType(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", smsCenters.getTotalElements());
        wsResponse.put("items", smsCenters.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<SmsCenter> smsCenters = null;
        if(deleted == DELETED){
            smsCenters = smsCenterRepository.findAllByIsDelete(pageable, 1);
        }else{
            smsCenters = smsCenterRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", smsCenters.getTotalElements());
        wsResponse.put("items", smsCenters.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(SmsCenter smsCenter, ResponseBean responseBean) throws Exception {
        SmsCenter oldSmsCenter = smsCenterRepository.findBySmsCenterId(smsCenter.getSmsCenterId());
        if(oldSmsCenter == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        smsCenterRepository.save(smsCenter);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(SmsCenter smsCenter, ResponseBean responseBean) throws Exception {
        smsCenter.setIsDelete(0);
        smsCenter.setSmsCenterId(null);
        smsCenterRepository.save(smsCenter);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long smsCenterId, Integer deleted, ResponseBean responseBean) throws Exception {
        SmsCenter smsCenter = smsCenterRepository.findBySmsCenterId(smsCenterId);
        if(smsCenter == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        smsCenter.setIsDelete(deleted);
        smsCenterRepository.save(smsCenter);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
