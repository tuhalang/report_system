package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Country;
import com.viettel.InterconnectSystem.repository.htds.CountryRepository;
import com.viettel.InterconnectSystem.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class CountryServiceImpl implements CountryService {

    private static final int DELETED = 1;

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public void searchByCountryName(Pageable pageable, ResponseBean responseBean, String key, Integer deleted) throws Exception{
        Page<Country> countryPage = null;
        if(deleted == DELETED){
            countryPage = countryRepository.findAllByIsDeleteAndCountryName(pageable, 1, key);
        }else{
            countryPage = countryRepository.findAllByIsDeleteAndCountryName(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", countryPage.getTotalElements());
        wsResponse.put("items", countryPage.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception{
        Page<Country> countryPage = null;
        if(deleted == DELETED){
            countryPage = countryRepository.findAllByIsDelete(pageable, 1);
        }else{
            countryPage = countryRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", countryPage.getTotalElements());
        wsResponse.put("items", countryPage.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(Country country, ResponseBean responseBean) throws Exception {
        Country oldCountry = countryRepository.findByCountryId(country.getCountryId());
        if(oldCountry == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        oldCountry.setCountryName(country.getCountryName());
        oldCountry.setCountryCode(country.getCountryCode());
        oldCountry.setAddPrefix(country.getAddPrefix());
        oldCountry.setCountryPrefixNumber(country.getCountryPrefixNumber());
        oldCountry.setDescription(country.getDescription());
        oldCountry.setDelete(country.getDelete());
        countryRepository.save(oldCountry);

        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(Country country, ResponseBean responseBean) throws Exception {
        country.setDelete(0);
        country.setCountryId(null);
        countryRepository.save(country);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long countryId, Integer deleted, ResponseBean responseBean) throws Exception {
        Country country = countryRepository.findByCountryId(countryId);
        if(country == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        country.setDelete(deleted);
        countryRepository.save(country);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
