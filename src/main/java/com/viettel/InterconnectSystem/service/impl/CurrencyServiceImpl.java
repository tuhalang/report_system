package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.model.htds.Currency;
import com.viettel.InterconnectSystem.repository.htds.CurrencyRepository;
import com.viettel.InterconnectSystem.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    CurrencyRepository currencyRepository;

    @Override
    public List<Currency> findAll() {
        return currencyRepository.findAll();
    }
}
