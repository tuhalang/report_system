package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Partner;
import com.viettel.InterconnectSystem.repository.htds.PartnerRepository;
import com.viettel.InterconnectSystem.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class PartnerServiceImpl implements PartnerService {

    private static final Integer DELETED = 1;

    @Autowired
    private PartnerRepository partnerRepository;

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<Partner> partnerPage = null;
        if(deleted == DELETED){
            partnerPage = partnerRepository.findAllByIsDelete(pageable, 1);
        }else{
            partnerPage = partnerRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", partnerPage.getTotalElements());
        wsResponse.put("items", partnerPage.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void searchByPartName(Pageable pageable, ResponseBean responseBean, String key, Integer deleted) throws Exception {
        Page<Partner> countryPage = null;
        if(deleted == DELETED){
            countryPage = partnerRepository.findAllByIsDeleteAndPartName(pageable, 1, key);
        }else{
            countryPage = partnerRepository.findAllByIsDeleteAndPartName(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", countryPage.getTotalElements());
        wsResponse.put("items", countryPage.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(Partner partner, ResponseBean responseBean) throws Exception {
        Partner oldPartner = partnerRepository.findByPartnerId(partner.getPartnerId());
        if(oldPartner == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Partner not found !");
            return;
        }
        oldPartner.setAddress(partner.getAddress());
        oldPartner.setCountryId(partner.getCountryId());
        oldPartner.setDelete(partner.getDelete());
        oldPartner.setFax(partner.getFax());
        oldPartner.setNational(partner.getNational());
        oldPartner.setPartAbbreviate(partner.getPartAbbreviate());
        oldPartner.setPartName(partner.getPartName());
        oldPartner.setPhone(partner.getPhone());
        oldPartner.setTimezoneInDiff(partner.getTimezoneInDiff());
        oldPartner.setTimezoneOutDiff(partner.getTimezoneOutDiff());
        oldPartner.setFax(partner.getFax());

        partnerRepository.save(oldPartner);

        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(Partner partner, ResponseBean responseBean) throws Exception {
        partner.setDelete(0);
        partner.setPartnerId(null);
        partnerRepository.save(partner);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long partnerId, Integer deleted, ResponseBean responseBean) throws Exception {
        Partner partner = partnerRepository.findByPartnerId(partnerId);
        if(partner == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Partner not found !");
            return;
        }
        partner.setDelete(deleted);
        partnerRepository.save(partner);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
