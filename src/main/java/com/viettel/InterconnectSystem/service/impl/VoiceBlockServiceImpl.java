package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.VoiceBlock;
import com.viettel.InterconnectSystem.repository.htds.VoiceBlockRepository;
import com.viettel.InterconnectSystem.service.VoiceBlockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class VoiceBlockServiceImpl implements VoiceBlockService {

    private static final int DELETED = 1;

    @Autowired
    private VoiceBlockRepository voiceBlockRepository;

    @Override
    public void searchByIsDeleteAndBlockName(Pageable pageable, ResponseBean responseBean, Integer deleted, String key) throws Exception {
        Page<VoiceBlock> voiceBlocks = null;
        if(deleted == DELETED){
            voiceBlocks = voiceBlockRepository.findByIsDeleteAndBlockName(pageable, 1, key);
        }else{
            voiceBlocks = voiceBlockRepository.findByIsDeleteAndBlockName(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", voiceBlocks.getTotalElements());
        wsResponse.put("items", voiceBlocks.getContent());
        responseBean.setData(wsResponse);
    }
}
