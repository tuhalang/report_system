package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.SwitchBoard;
import com.viettel.InterconnectSystem.repository.htds.SwitchBoardRepository;
import com.viettel.InterconnectSystem.service.SwitchBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class SwitchBoardServiceImpl implements SwitchBoardService {

    private static final int DELETED = 1;

    @Autowired
    private SwitchBoardRepository switchBoardRepository;

    @Override
    public void searchByIsDeleteAndSwitchName(Pageable pageable, ResponseBean responseBean,
                                              Integer deleted, String switchName) throws Exception{
        Page<SwitchBoard> switchBoards = null;
        if(deleted == DELETED){
            switchBoards = switchBoardRepository.findAllByIsDeleteAndSwitchName(pageable, 1, switchName);
        }else{
            switchBoards = switchBoardRepository.findAllByIsDeleteAndSwitchName(pageable, 0, switchName);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", switchBoards.getTotalElements());
        wsResponse.put("items", switchBoards.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<SwitchBoard> switchBoards = null;
        if(deleted == DELETED){
            switchBoards = switchBoardRepository.findAllByIsDelete(pageable, 1);
        }else{
            switchBoards = switchBoardRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", switchBoards.getTotalElements());
        wsResponse.put("items", switchBoards.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(SwitchBoard switchBoard, ResponseBean responseBean) throws Exception {
        SwitchBoard oldSwitchBoard = switchBoardRepository.findBySwitchId(switchBoard.getSwitchId());
        if(oldSwitchBoard == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        oldSwitchBoard.setIsDelete(switchBoard.getIsDelete());
        oldSwitchBoard.setAreaId(switchBoard.getAreaId());
        oldSwitchBoard.setCompany(switchBoard.getCompany());
        oldSwitchBoard.setPlace(switchBoard.getPlace());
        oldSwitchBoard.setShortFor(switchBoard.getShortFor());
        oldSwitchBoard.setSwitchName(switchBoard.getSwitchName());
        oldSwitchBoard.setSwitchType(switchBoard.getSwitchType());

        switchBoardRepository.save(oldSwitchBoard);

        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(SwitchBoard switchBoard, ResponseBean responseBean) throws Exception {
        switchBoard.setIsDelete(0);
        switchBoard.setSwitchId(null);
        switchBoardRepository.save(switchBoard);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long switchBoardId, Integer deleted, ResponseBean responseBean) throws Exception {
        SwitchBoard switchBoard = switchBoardRepository.findBySwitchId(switchBoardId);
        if(switchBoard == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("SwitchBoard not found !");
            return;
        }
        switchBoard.setIsDelete(deleted);
        switchBoardRepository.save(switchBoard);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
