package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.ResponseBean;
import com.viettel.InterconnectSystem.model.htds.Route;
import com.viettel.InterconnectSystem.repository.htds.RouteRepository;
import com.viettel.InterconnectSystem.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service
public class RouteServiceImpl implements RouteService {

    private static final int DELETED = 1;

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public void searchByIsDeleteAndRouteName(Pageable pageable, ResponseBean responseBean,
                                             Integer deleted, String key) throws Exception{
        Page<Route> routes = null;
        if(deleted == DELETED){
            routes = routeRepository.findAllByIsDeleteAndRouteName(pageable, 1, key);
        }else{
            routes = routeRepository.findAllByIsDeleteAndRouteName(pageable, 0, key);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", routes.getTotalElements());
        wsResponse.put("items", routes.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void findAll(Pageable pageable, ResponseBean responseBean, Integer deleted) throws Exception {
        Page<Route> routes = null;
        if(deleted == DELETED){
            routes = routeRepository.findAllByIsDelete(pageable, 1);
        }else{
            routes = routeRepository.findAllByIsDelete(pageable, 0);
        }
        responseBean.setErrorCode("0");
        responseBean.setMsg("Successfully !");
        LinkedHashMap<String, Object> wsResponse = new LinkedHashMap<>();
        wsResponse.put("total", routes.getTotalElements());
        wsResponse.put("items", routes.getContent());
        responseBean.setData(wsResponse);
    }

    @Override
    public void update(Route route, ResponseBean responseBean) throws Exception {
        Route oldRoute = routeRepository.findByRouteId(route.getRouteId());
        if(oldRoute == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Country not found !");
            return;
        }
        Route r = routeRepository.findByRouteNameAndServiceId(route.getRouteName(), route.getSwitchId());
        if(oldRoute.getSwitchId() != route.getSwitchId() && r != null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Route name and switch are exists !");
            return;
        }
        routeRepository.save(route);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Update successfully !");
    }

    @Override
    public void create(Route route, ResponseBean responseBean) throws Exception {
        Route r = routeRepository.findByRouteNameAndServiceId(route.getRouteName(), route.getSwitchId());
        if(r != null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Route name and switch are exists !");
            return;
        }
        route.setIsDelete(0);
        route.setRouteId(null);
        routeRepository.save(route);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Create successfully !");
    }

    @Override
    public void delete(Long routeId, Integer deleted, ResponseBean responseBean) throws Exception {
        Route route = routeRepository.findByRouteId(routeId);
        if(route == null){
            responseBean.setErrorCode("1");
            responseBean.setMsg("Service not found !");
            return;
        }
        route.setIsDelete(deleted);
        routeRepository.save(route);
        responseBean.setErrorCode("0");
        responseBean.setMsg("Delete successfully !");
    }
}
