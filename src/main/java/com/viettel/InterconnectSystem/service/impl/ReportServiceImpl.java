package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.model.inter.ReportCategory;
import com.viettel.InterconnectSystem.repository.htds.FullPreInterNumberRepository;
import com.viettel.InterconnectSystem.repository.inter.ReportCategoryRepository;
import com.viettel.InterconnectSystem.repository.inter.ReportRepository;
import com.viettel.InterconnectSystem.service.ReportService;
import com.viettel.InterconnectSystem.utils.ExcelUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ReportCategoryRepository reportCategoryRepository;

    @Autowired
    private FullPreInterNumberRepository fullPreInterNumberRepository;

    @Override
    public ByteArrayInputStream getReportData(Long categoryId, Long partnerId, Date date) {
        ReportCategory reportCategory = reportCategoryRepository.findByReportId(categoryId);
        List datas = reportRepository.getReportData(reportCategory.getExcutePathScript(), reportCategory.getReportTypeId(), partnerId, date);

        String header = reportCategory.getHeaders();

        String[] headersName = header.split(",");
        try {
            return ExcelUtils.generateExcelFile(headersName, datas);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public ByteArrayInputStream getInterPrice(Long partnerId, String interType, String direction) {
        List datas = fullPreInterNumberRepository.findAllByPartnerIdAndInterTypeAndDirection(partnerId, interType, direction);
        String[] headers = new String[]{"Destination", "Numbering plan", "Rates per minute", "Rate valid from"};
        try{
            return ExcelUtils.generateInterPrice(headers, datas);
        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }
        return null;
    }
}
