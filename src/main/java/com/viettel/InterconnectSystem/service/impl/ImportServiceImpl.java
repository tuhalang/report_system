package com.viettel.InterconnectSystem.service.impl;

import com.viettel.InterconnectSystem.controller.bean.FileUpload;
import com.viettel.InterconnectSystem.controller.bean.PriceInfo;
import com.viettel.InterconnectSystem.model.htds.InterCodePrice;
import com.viettel.InterconnectSystem.model.htds.PreInterNumGroup;
import com.viettel.InterconnectSystem.model.htds.PrefixInterNumber;
import com.viettel.InterconnectSystem.repository.htds.InterCodePriceRepository;
import com.viettel.InterconnectSystem.repository.htds.PreInterNumGroupRepository;
import com.viettel.InterconnectSystem.repository.htds.PrefixInterNumberRepository;
import com.viettel.InterconnectSystem.service.ImportService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class ImportServiceImpl implements ImportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImportServiceImpl.class);

    private static final Integer BATCH_SIZE = 500;

    @Autowired
    private InterCodePriceRepository interCodePriceRepository;

    @Autowired
    private PreInterNumGroupRepository preInterNumGroupRepository;

    @Autowired
    private PrefixInterNumberRepository prefixInterNumberRepository;

    @Override
    public void importPrice(FileUpload fileUpload) throws Exception {

        LOGGER.info("START IMPORT PRICE");

        InterCodePrice interCodePrice = interCodePriceRepository.findByPartnerIdAndInterType(fileUpload.getPartnerId(), fileUpload.getInterType());

        if(interCodePrice == null) {
            interCodePrice = new InterCodePrice();
            interCodePrice.setCodePriceName(fileUpload.getCodePriceName());
            interCodePrice.setConnectType(fileUpload.getConnectType());
            interCodePrice.setDateCreate(fileUpload.getDateCreate());
            interCodePrice.setDirection(fileUpload.getDirection());
            interCodePrice.setInterType(fileUpload.getInterType());
            interCodePrice.setIsDelete(fileUpload.getIsDelete());
            interCodePrice.setPriceTableType(fileUpload.getPriceTableType());
            interCodePrice.setPartnerId(fileUpload.getPartnerId());
            interCodePrice.setPartType(fileUpload.getPartType());
            interCodePrice.setIsDelete(0);
            interCodePrice = interCodePriceRepository.save(interCodePrice);
            LOGGER.info("INSERT INTO INTER_CODE_PRICE: " + interCodePrice.getInterCodePriceId());
        }else{
            interCodePrice.setPartType(fileUpload.getPartType());
            interCodePrice.setDirection(fileUpload.getDirection());
            interCodePrice.setCodePriceName(fileUpload.getCodePriceName());
            interCodePrice.setConnectType(fileUpload.getConnectType());
            interCodePrice.setDateCreate(new Date());
            interCodePrice = interCodePriceRepository.save(interCodePrice);
            LOGGER.info("UPDATE INTO INTER_CODE_PRICE: " + interCodePrice.getInterCodePriceId());
        }

        LOGGER.info("START LOAD FROM EXCEL FILE");
        InputStream inputStream = new FileInputStream(fileUpload.getFile());
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        XSSFSheet worksheet = workbook.getSheetAt(0);


        HashMap<String, ArrayList<PriceInfo>> priceInfoHashMap = new HashMap<>();
        for(int i= fileUpload.getRowStart()-1; i<fileUpload.getRowEnd(); i++) {
            try {
                XSSFRow xssfRow = worksheet.getRow(i);

                Cell cellDestination = xssfRow.getCell(fileUpload.getDestination());
                cellDestination.setCellType(Cell.CELL_TYPE_STRING);
                String destination = cellDestination.getStringCellValue().trim();

                Cell cellAreaCode = xssfRow.getCell(fileUpload.getAreaCode());
                cellAreaCode.setCellType(Cell.CELL_TYPE_STRING);
                String areaCodeStr = cellAreaCode.getStringCellValue().trim();
                String[] areaCodes = areaCodeStr.split(",");

                Cell cellRate = xssfRow.getCell(fileUpload.getRate());
                cellRate.setCellType(Cell.CELL_TYPE_NUMERIC);
                Double rate = cellRate.getNumericCellValue();

                Cell cellRateValid = xssfRow.getCell(fileUpload.getRateValid());
                cellRateValid.setCellType(Cell.CELL_TYPE_NUMERIC);
                Date rateValid = cellRateValid.getDateCellValue();

                PriceInfo priceInfo = new PriceInfo(areaCodes, rate, rateValid);

                if (priceInfoHashMap.containsKey(destination)) {
                    ArrayList<PriceInfo> priceInfos = priceInfoHashMap.get(destination);
                    priceInfos.add(priceInfo);
                    priceInfoHashMap.put(destination, priceInfos);
                } else {
                    ArrayList<PriceInfo> priceInfos = new ArrayList<>();
                    priceInfos.add(priceInfo);
                    priceInfoHashMap.put(destination, priceInfos);
                }
            }catch (Exception e){
                LOGGER.error(e.getMessage(), e);
                LOGGER.error("Error in get row " + i);
            }
        }


        List<PreInterNumGroup> preInterNumGroups = new ArrayList<>();
        List<PrefixInterNumber> prefixInterNumbers = new ArrayList<>();
        BigDecimal seq =  null;
        for(String destination : priceInfoHashMap.keySet()){
            ArrayList<PriceInfo> priceInfos = priceInfoHashMap.get(destination);
            for(PriceInfo priceInfo : priceInfos) {
                String[] areaCodes = priceInfo.getAreaCodes();
                Double rate = priceInfo.getRate();
                Date rateValid = priceInfo.getRateValid();

                PreInterNumGroup preInterNumGroup = preInterNumGroupRepository
                        .findByPartnerIdAndNumberGroupNameAndInterCodePriceId(fileUpload.getPartnerId(),
                                destination, interCodePrice.getInterCodePriceId());

                if (preInterNumGroup == null) {
                    seq = preInterNumGroupRepository.getNextSequence();
                    preInterNumGroup = new PreInterNumGroup();
                    preInterNumGroup.setPreInterNumGroupId(seq);
                    preInterNumGroup.setInterCodePriceId(interCodePrice.getInterCodePriceId());
                    preInterNumGroup.setNumberGroupName(destination);
                    preInterNumGroup.setPartnerId(fileUpload.getPartnerId());
                    preInterNumGroup.setIsDelete(0);
                    preInterNumGroups.add(preInterNumGroup);
                }

                for (String areaCode : areaCodes) {
                    areaCode = areaCode.trim();
                    PrefixInterNumber prefixInterNumber = prefixInterNumberRepository
                            .findByPreInterNumGroupIdAndPrefixNumber(preInterNumGroup.getPreInterNumGroupId(), areaCode);

                    if (prefixInterNumber == null) {
                        prefixInterNumber = new PrefixInterNumber();
                        prefixInterNumber.setPartnerId(fileUpload.getPartnerId());
                        prefixInterNumber.setPartType(fileUpload.getPartType());
                        prefixInterNumber.setPrefixNumber(areaCode);
                        prefixInterNumber.setLengthNumber(areaCode.length());
                        prefixInterNumber.setNumberFunction(2);
                        prefixInterNumber.setPreInterNumGroupId(preInterNumGroup.getPreInterNumGroupId());
                        prefixInterNumber.setIsDelete(0);
                        prefixInterNumber.setDateApply(rateValid);
                        prefixInterNumber.setPrice0(rate);
                        prefixInterNumber.setCurrencyId(fileUpload.getCurrencyId());
                        prefixInterNumbers.add(prefixInterNumber);
                    } else {
                        prefixInterNumber.setDateApply(rateValid);
                        prefixInterNumber.setPrice0(rate);
                        prefixInterNumbers.add(prefixInterNumber);
                    }
                }
            }
        }
        preInterNumGroupRepository.saveAll(preInterNumGroups);
        prefixInterNumberRepository.saveAll(prefixInterNumbers);
    }
}
