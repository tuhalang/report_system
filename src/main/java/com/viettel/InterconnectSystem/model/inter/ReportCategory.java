package com.viettel.InterconnectSystem.model.inter;

import javax.persistence.*;

@Entity
@Table(name = "REPORT_CATEGORY")
public class ReportCategory {

    @Id
    @GeneratedValue(generator = "REPORT_CATEGORY_SEQ")
    @SequenceGenerator(schema = "INTERCONNECT",
            name = "REPORT_CATEGORY_SEQ",
            sequenceName = "REPORT_CATEGORY_SEQ",
            allocationSize = 1)
    @Column(name = "REPORT_ID")
    private Long reportId;

    @Column(name = "REPORT_NAME")
    private String reportName;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "EXCUTE_PATH_SCRIPT")
    private String excutePathScript;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "REPORT_TYPE_ID")
    private Long reportTypeId;

    @Column(name = "HEADERS")
    private String headers;

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public Long getReportTypeId() {
        return reportTypeId;
    }

    public void setReportTypeId(Long reportTypeId) {
        this.reportTypeId = reportTypeId;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExcutePathScript() {
        return excutePathScript;
    }

    public void setExcutePathScript(String excutePathSctipr) {
        this.excutePathScript = excutePathSctipr;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
