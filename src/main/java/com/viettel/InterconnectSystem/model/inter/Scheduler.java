package com.viettel.InterconnectSystem.model.inter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SCHEDULER")
public class Scheduler {

    @Id
    @GeneratedValue(generator = "SCHEDULER_SEQ")
    @SequenceGenerator(schema = "INTERCONNECT",
            name = "SCHEDULER_SEQ",
            sequenceName = "SCHEDULER_SEQ",
            allocationSize = 1)
    @Column(name = "SYNTH_TYPE_ID")
    private Long synthTypeId;

    @Column(name = "START_DATETIME")
    private Date startDatetime;

    @Column(name = "DELTA")
    private Integer delta;

    @Column(name = "DEEP_LOOKUP")
    private Integer deepLookup;

    @Column(name = "PRIORITY")
    private Integer priority;

    @Column(name = "STATUS")
    private Integer status;

    @Column(name = "DESCRIPTION")
    private String description;

    public Long getSynthTypeId() {
        return synthTypeId;
    }

    public void setSynthTypeId(Long synthTypeId) {
        this.synthTypeId = synthTypeId;
    }

    public Date getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    public Integer getDelta() {
        return delta;
    }

    public void setDelta(Integer delta) {
        this.delta = delta;
    }

    public Integer getDeepLookup() {
        return deepLookup;
    }

    public void setDeepLookup(Integer deepLookup) {
        this.deepLookup = deepLookup;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
