package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ROUTE")
public class Route {

    @Id
    @GeneratedValue(generator = "ROUTE_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "ROUTE_SEQ",
            sequenceName = "ROUTE_SEQ",
            allocationSize = 1)
    @Column(name = "ROUTE_ID")
    private Long routeId;

    @Column(name = "ROUTE_NAME")
    private String routeName;

    @Column(name = "SWITCH_ID")
    private Long switchId;

    @Column(name = "PREFIX_NUMBER_ID")
    private Long prefixNumberId;

    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "PART_TYPE")
    private String partType;

    @Column(name = "CONNECT_SWITCH_TYPE")
    private String connectSwitchType;

    @Column(name = "CONNECT_AREA_ID")
    private Long connectAreaId;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "SERVICE_ID")
    private Long serviceId;

    @Column(name = "DIRECTION_NAME")
    private String directionName;

    @Column(name = "UPDATE_TIME")
    private Date updateTime;

    @Column(name = "ISDN")
    private String isdn;

    @Column(name = "KHTK_NAME")
    private String khtkName;

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public Long getSwitchId() {
        return switchId;
    }

    public void setSwitchId(Long switchID) {
        this.switchId = switchID;
    }

    public Long getPrefixNumberId() {
        return prefixNumberId;
    }

    public void setPrefixNumberId(Long prefixNumberId) {
        this.prefixNumberId = prefixNumberId;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public String getConnectSwitchType() {
        return connectSwitchType;
    }

    public void setConnectSwitchType(String connectSwitchType) {
        this.connectSwitchType = connectSwitchType;
    }

    public Long getConnectAreaId() {
        return connectAreaId;
    }

    public void setConnectAreaId(Long connectAreaId) {
        this.connectAreaId = connectAreaId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getDirectionName() {
        return directionName;
    }

    public void setDirectionName(String directionName) {
        this.directionName = directionName;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsdn() {
        return isdn;
    }

    public void setIsdn(String isdn) {
        this.isdn = isdn;
    }

    public String getKhtkName() {
        return khtkName;
    }

    public void setKhtkName(String khtkName) {
        this.khtkName = khtkName;
    }
}
