package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "PREFIX_NUMBER")
public class PrefixNumber {

    @Id
    @GeneratedValue(generator = "PREFIX_NUMBER_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "PREFIX_NUMBER_SEQ",
            sequenceName = "PREFIX_NUMBER_SEQ",
            allocationSize = 1)
    @Column(name = "PREFIX_NUMBER_ID")
    private Long prefixNumberId;

    @Column(name = "SERVICE_ID")
    private Long serviceId;

    @Column(name = "PREFIX_NUMBER")
    private String prefixNumber;

    @Column(name = "LENGTH_NUMBER")
    private Integer lengthNumber;

    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "PART_TYPE")
    private String partType;

    @Column(name = "AREA_PREFIX_NUMBER")
    private String areaPrefixNumber;

    @Column(name = "AREA_ID")
    private Long areaId;

    @Column(name = "COUNTRY_ID")
    private Long countryId;

    @Column(name = "CODE_TYPE")
    private String codeType;

    @Column(name = "NUMBER_FUNCTION")
    private Integer numberFunction;

    @Column(name = "ADD_PREFIX")
    private String addPrefix;

    @Column(name = "VALID_PREFIX_NUMBER")
    private String validPrefixNumber;

    @Column(name = "VALID_PREFIX_NUMBER_ID")
    private Long validPrefixNumberId;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "SUB_PART_TYPE")
    private String subPartType;

    @Column(name = "SERVER_NUMBER")
    private String serverNumber;

    public Long getPrefixNumberId() {
        return prefixNumberId;
    }

    public void setPrefixNumberId(Long prefixNumberId) {
        this.prefixNumberId = prefixNumberId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(String prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public Integer getLengthNumber() {
        return lengthNumber;
    }

    public void setLengthNumber(Integer lengthNumber) {
        this.lengthNumber = lengthNumber;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public String getAreaPrefixNumber() {
        return areaPrefixNumber;
    }

    public void setAreaPrefixNumber(String areaPrefixNumber) {
        this.areaPrefixNumber = areaPrefixNumber;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public Integer getNumberFunction() {
        return numberFunction;
    }

    public void setNumberFunction(Integer numberFunction) {
        this.numberFunction = numberFunction;
    }

    public String getAddPrefix() {
        return addPrefix;
    }

    public void setAddPrefix(String addPrefix) {
        this.addPrefix = addPrefix;
    }

    public String getValidPrefixNumber() {
        return validPrefixNumber;
    }

    public void setValidPrefixNumber(String validPrefixNumber) {
        this.validPrefixNumber = validPrefixNumber;
    }

    public Long getValidPrefixNumberId() {
        return validPrefixNumberId;
    }

    public void setValidPrefixNumberId(Long validPrefixNumberId) {
        this.validPrefixNumberId = validPrefixNumberId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getSubPartType() {
        return subPartType;
    }

    public void setSubPartType(String subPartType) {
        this.subPartType = subPartType;
    }

    public String getServerNumber() {
        return serverNumber;
    }

    public void setServerNumber(String serverNumber) {
        this.serverNumber = serverNumber;
    }
}
