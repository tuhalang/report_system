package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "CONVERT_PROCESS_PARAM")
public class ConvertProcessParam {

    @Id
    @GeneratedValue(generator = "CONVERT_PROCESS_PARAM_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "CONVERT_PROCESS_PARAM_SEQ",
            sequenceName = "CONVERT_PROCESS_PARAM_SEQ",
            allocationSize = 1)
    @Column(name = "CONVERT_ID")
    private Long convertId;

    @Column(name = "DOMAIN_CODE")
    private String domainCode;

    @Column(name = "PROCESS_CODE")
    private String processCode;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "INPUT_DIR")
    private String inputDir;

    @Column(name = "OUTPUT_DIR")
    private String outputDir;

    @Column(name = "BACKUP_DIR")
    private String backupDir;

    @Column(name = "ERROR_DIR")
    private String errorDir;

    @Column(name = "MIN_SEQ")
    private Integer minSeq;

    @Column(name = "MAX_SEQ")
    private Integer maxSeq;

    @Column(name = "CDR_FILE_NAME_TEMPLATE")
    private String cdrFileNameTemplate;

    @Column(name = "STOP_WHEN_ERROR")
    private Integer stopWhenError;

    @Column(name = "WARNING_TYPE")
    private Integer warningType;

    @Column(name = "DELAY_TIME")
    private Integer delayTime;

    @Column(name = "LOG_FILE_PATH")
    private String logFilePath;

    @Column(name = "CDR_TYPE")
    private Integer cdrType;

    @Column(name = "CONVERT_CLASS_NAME")
    private String convertClassName;

    @Column(name = "CONVERT_PARAM_PATH")
    private String convertParamPath;

    @Column(name = "TXT_FILE_NAME_TEMPLATE")
    private String txtFileNameTemplate;

    @Column(name = "TIME_TYPE")
    private Integer timeType;

    @Column(name = "TIME_DELAY")
    private Integer timeDelay;

    @Column(name = "IS_BACKUP")
    private Integer isBackup;

    @Column(name = "SWITCH_ID")
    private Long switchId;

    @Column(name = "IS_DELETE")
    private Integer isDelete;


    public Long getConvertId() {
        return convertId;
    }

    public void setConvertId(Long convertId) {
        this.convertId = convertId;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInputDir() {
        return inputDir;
    }

    public void setInputDir(String inputDir) {
        this.inputDir = inputDir;
    }

    public String getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    public String getBackupDir() {
        return backupDir;
    }

    public void setBackupDir(String backupDir) {
        this.backupDir = backupDir;
    }

    public String getErrorDir() {
        return errorDir;
    }

    public void setErrorDir(String errorDir) {
        this.errorDir = errorDir;
    }

    public Integer getMinSeq() {
        return minSeq;
    }

    public void setMinSeq(Integer minSeq) {
        this.minSeq = minSeq;
    }

    public Integer getMaxSeq() {
        return maxSeq;
    }

    public void setMaxSeq(Integer maxSeq) {
        this.maxSeq = maxSeq;
    }

    public String getCdrFileNameTemplate() {
        return cdrFileNameTemplate;
    }

    public void setCdrFileNameTemplate(String cdrFileNameTemplate) {
        this.cdrFileNameTemplate = cdrFileNameTemplate;
    }

    public Integer getStopWhenError() {
        return stopWhenError;
    }

    public void setStopWhenError(Integer stopWhenError) {
        this.stopWhenError = stopWhenError;
    }

    public Integer getWarningType() {
        return warningType;
    }

    public void setWarningType(Integer warningType) {
        this.warningType = warningType;
    }

    public Integer getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(Integer delayTime) {
        this.delayTime = delayTime;
    }

    public String getLogFilePath() {
        return logFilePath;
    }

    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    public Integer getCdrType() {
        return cdrType;
    }

    public void setCdrType(Integer cdrType) {
        this.cdrType = cdrType;
    }

    public String getConvertClassName() {
        return convertClassName;
    }

    public void setConvertClassName(String convertClassName) {
        this.convertClassName = convertClassName;
    }

    public String getConvertParamPath() {
        return convertParamPath;
    }

    public void setConvertParamPath(String convertParamPath) {
        this.convertParamPath = convertParamPath;
    }

    public String getTxtFileNameTemplate() {
        return txtFileNameTemplate;
    }

    public void setTxtFileNameTemplate(String txtFileNameTemplate) {
        this.txtFileNameTemplate = txtFileNameTemplate;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Integer getTimeDelay() {
        return timeDelay;
    }

    public void setTimeDelay(Integer timeDelay) {
        this.timeDelay = timeDelay;
    }

    public Integer getIsBackup() {
        return isBackup;
    }

    public void setIsBackup(Integer isBackup) {
        this.isBackup = isBackup;
    }

    public Long getSwitchId() {
        return switchId;
    }

    public void setSwitchId(Long switchId) {
        this.switchId = switchId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
