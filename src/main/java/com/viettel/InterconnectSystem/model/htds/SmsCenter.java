package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "SMS_CENTRE")
public class SmsCenter {

    @Id
    @GeneratedValue(generator = "SMS_CENTRE_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "SMS_CENTRE_SEQ",
            sequenceName = "SMS_CENTRE_SEQ",
            allocationSize = 1)
    @Column(name = "SMS_CENTRE_ID")
    private Long smsCenterId;

    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "PART_TYPE")
    private String partType;

    @Column(name = "PREFIX_NUMBER")
    private String prefixNumber;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    public Long getSmsCenterId() {
        return smsCenterId;
    }

    public void setSmsCenterId(Long smsCenterId) {
        this.smsCenterId = smsCenterId;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public String getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(String prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
