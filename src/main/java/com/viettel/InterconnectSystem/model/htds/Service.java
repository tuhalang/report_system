package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "SERVICE")
public class Service {

    @Id
    @GeneratedValue(generator = "SERVICE_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "SERVICE_SEQ",
            sequenceName = "SERVICE_SEQ",
            allocationSize = 1)
    @Column(name = "SERVICE_ID")
    private Long serviceId;

    @Column(name = "SERVICE_NAME")
    private String serviceName;

    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "VOICE_BLOCK_ID")
    private Long voiceBlockId;

    @Column(name = "SERVICE_TYPE")
    private String serviceType;

    @Column(name = "BLOCK_TYPE")
    private String blockType;

    @Column(name = "TIME_OFF_PEAK_ID")
    private Long timeOffPeakId;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "SERVICE_CODE")
    private String serviceCode;

    @Column(name = "SWITCH_ID")
    private Long switchId;

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getVoiceBlockId() {
        return voiceBlockId;
    }

    public void setVoiceBlockId(Long voiceBlockId) {
        this.voiceBlockId = voiceBlockId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getBlockType() {
        return blockType;
    }

    public void setBlockType(String blockType) {
        this.blockType = blockType;
    }

    public Long getTimeOffPeakId() {
        return timeOffPeakId;
    }

    public void setTimeOffPeakId(Long timeOffPeakId) {
        this.timeOffPeakId = timeOffPeakId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public Long getSwitchId() {
        return switchId;
    }

    public void setSwitchId(Long switchId) {
        this.switchId = switchId;
    }
}
