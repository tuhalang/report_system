package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "CURRENCY")
public class Currency {

    @Id
    @GeneratedValue(generator = "CURRENCY_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "CURRENCY_SEQ",
            sequenceName = "CURRENCY_SEQ",
            allocationSize = 1)
    @Column(name = "CURRENCY_ID")
    private Long currencyId;

    @Column(name = "CURRENCY_NAME")
    private String currencyName;

    @Column(name = "CURRENCY_SIGN")
    private String currencySign;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencySign() {
        return currencySign;
    }

    public void setCurrencySign(String currencySign) {
        this.currencySign = currencySign;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
