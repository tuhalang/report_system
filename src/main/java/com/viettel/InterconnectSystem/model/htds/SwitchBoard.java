package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "SWITCHBOARD")
public class SwitchBoard {

    @Id
    @GeneratedValue(generator = "SWITCHBOARD_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "SWITCHBOARD_SEQ",
            sequenceName = "SWITCHBOARD_SEQ",
            allocationSize = 1)
    @Column(name = "SWITCH_ID")
    private Long switchId;

    @Column(name = "SWITCH_NAME")
    private String switchName;

    @Column(name = "PLACE")
    private String place;

    @Column(name = "COMPANY")
    private String company;

    @Column(name = "SWITCH_TYPE")
    private String switchType;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "SHORT_FOR")
    private Integer shortFor;

    @Column(name = "AREA_ID")
    private Long areaId;

    public Long getSwitchId() {
        return switchId;
    }

    public void setSwitchId(Long switchId) {
        this.switchId = switchId;
    }

    public String getSwitchName() {
        return switchName;
    }

    public void setSwitchName(String switchName) {
        this.switchName = switchName;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSwitchType() {
        return switchType;
    }

    public void setSwitchType(String switchType) {
        this.switchType = switchType;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getShortFor() {
        return shortFor;
    }

    public void setShortFor(Integer shortFor) {
        this.shortFor = shortFor;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
}
