package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "COUNTRY")
public class Country {

    @Id
    @GeneratedValue(generator = "COUNTRY_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "COUNTRY_SEQ",
            sequenceName = "COUNTRY_SEQ",
            allocationSize = 1)
    @Column(name = "COUNTRY_ID")
    private Long countryId;

    @Column(name = "COUNTRY_NAME", updatable = false)
    private String countryName;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "COUNTRY_PREFIX_NUMBER")
    private String countryPrefixNumber;

    @Column(name = "ADD_PREFIX")
    private String addPrefix;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryPrefixNumber() {
        return countryPrefixNumber;
    }

    public void setCountryPrefixNumber(String countryPrefixNumber) {
        this.countryPrefixNumber = countryPrefixNumber;
    }

    public String getAddPrefix() {
        return addPrefix;
    }

    public void setAddPrefix(String addPrefix) {
        this.addPrefix = addPrefix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDelete() {
        return isDelete;
    }

    public void setDelete(Integer delete) {
        isDelete = delete;
    }
}
