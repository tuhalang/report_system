package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "PARTNER")
public class Partner {


    @Id
    @GeneratedValue(generator = "PARTNER_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "PARTNER_SEQ",
            sequenceName = "PARTNER_SEQ",
            allocationSize = 1)
    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "COUNTRY_ID", nullable = false)
    private Long countryId;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "FAX")
    private String fax;

    @Column(name = "PART_NAME")
    private String partName;

    @Column(name = "PART_ABBREVIATE")
    private String partAbbreviate;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "IS_NATIONAL")
    private Integer isNational;

    @Column(name = "IS_VAS")
    private Integer isVas;

    @Column(name = "TIMEZONE_OUT_DIFF")
    private Integer timezoneOutDiff;

    @Column(name = "TIMEZONE_IN_DIFF")
    private Integer timezoneInDiff;

    public Partner() {
    }

    public Partner(Long partnerId, Long countryId, String address, String phone, String fax, String partName, String partAbbreviate, Integer isDelete, Integer isNational, Integer isVas, Integer timezoneOutDiff, Integer timezoneInDiff) {
        this.partnerId = partnerId;
        this.countryId = countryId;
        this.address = address;
        this.phone = phone;
        this.fax = fax;
        this.partName = partName;
        this.partAbbreviate = partAbbreviate;
        this.isDelete = isDelete;
        this.isNational = isNational;
        this.isVas = isVas;
        this.timezoneOutDiff = timezoneOutDiff;
        this.timezoneInDiff = timezoneInDiff;
    }

    public Integer getDelete() {
        return isDelete;
    }

    public Integer getNational() {
        return isNational;
    }

    public Integer getVas() {
        return isVas;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getPartAbbreviate() {
        return partAbbreviate;
    }

    public void setPartAbbreviate(String partAbbreviate) {
        this.partAbbreviate = partAbbreviate;
    }

    public Integer isDelete() {
        return isDelete;
    }

    public void setDelete(Integer delete) {
        isDelete = delete;
    }

    public Integer isNational() {
        return isNational;
    }

    public void setNational(Integer national) {
        isNational = national;
    }

    public Integer isVas() {
        return isVas;
    }

    public void setVas(Integer vas) {
        isVas = vas;
    }

    public Integer getTimezoneOutDiff() {
        return timezoneOutDiff;
    }

    public void setTimezoneOutDiff(Integer timezoneOutDiff) {
        this.timezoneOutDiff = timezoneOutDiff;
    }

    public Integer getTimezoneInDiff() {
        return timezoneInDiff;
    }

    public void setTimezoneInDiff(Integer timezoneInDiff) {
        this.timezoneInDiff = timezoneInDiff;
    }
}
