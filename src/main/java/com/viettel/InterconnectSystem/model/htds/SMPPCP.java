package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "SMPP_CP")
public class SMPPCP {

    @Id
    @GeneratedValue(generator = "SMPP_CP_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "SMPP_CP_SEQ",
            sequenceName = "SMPP_CP_SEQ",
            allocationSize = 1)
    @Column(name = "SMPP_CP_ID")
    private Long smppCpId;

    @Column(name = "SMPP_CP_NAME")
    private String smppCpName;

    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "SWITCH_ID")
    private Long switchId;

    @Column(name = "PREFIX_NUMBER")
    private String prefixNumber;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    public Long getSmppCpId() {
        return smppCpId;
    }

    public void setSmppCpId(Long smppCpId) {
        this.smppCpId = smppCpId;
    }

    public String getSmppCpName() {
        return smppCpName;
    }

    public void setSmppCpName(String smppCpName) {
        this.smppCpName = smppCpName;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getSwitchId() {
        return switchId;
    }

    public void setSwitchId(Long switchId) {
        this.switchId = switchId;
    }

    public String getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(String prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
