package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "INTER_CODE_PRICE")
public class InterCodePrice {

    @Id
    @Column(name = "INTER_CODE_PRICE_ID")
    @GeneratedValue(generator = "INTER_CODE_PRICE_SEQ")
    @SequenceGenerator(schema = "HTDS", name = "INTER_CODE_PRICE_SEQ", sequenceName = "INTER_CODE_PRICE_SEQ", allocationSize = 1)
    private Long interCodePriceId;

    @Column(name = "INTER_TYPE")
    private String interType;

    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "PART_TYPE")
    private String partType;

    @Column(name = "DATE_CREATE")
    private Date dateCreate;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "DIRECTION")
    private String direction;

    @Column(name = "CODE_PRICE_NAME")
    private String codePriceName;

    @Column(name = "CONNECT_TYPE")
    private String connectType;

    @Column(name = "PRICE_TABLE_TYPE")
    private String priceTableType;

    public Long getInterCodePriceId() {
        return interCodePriceId;
    }

    public void setInterCodePriceId(Long interCodePriceId) {
        this.interCodePriceId = interCodePriceId;
    }

    public String getInterType() {
        return interType;
    }

    public void setInterType(String interType) {
        this.interType = interType;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCodePriceName() {
        return codePriceName;
    }

    public void setCodePriceName(String codePriceName) {
        this.codePriceName = codePriceName;
    }

    public String getConnectType() {
        return connectType;
    }

    public void setConnectType(String connectType) {
        this.connectType = connectType;
    }

    public String getPriceTableType() {
        return priceTableType;
    }

    public void setPriceTableType(String priceTableType) {
        this.priceTableType = priceTableType;
    }
}
