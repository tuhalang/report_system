package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;

@Entity
@Table(name = "STANDARDIZE_PROCESS_PARAM")
public class StandardizeProcessParam {

    @Id
    @GeneratedValue(generator = "STANDARDIZE_PROCESS_PARAM_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "STANDARDIZE_PROCESS_PARAM_SEQ",
            sequenceName = "STANDARDIZE_PROCESS_PARAM_SEQ",
            allocationSize = 1)
    @Column(name = "STANDARDIZE_ID")
    private Long standardizeId;

    @Column(name = "DOMAIN_CODE")
    private String domainCode;

    @Column(name = "PROCESS_CODE")
    private String processCode;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "TXT_FILE_NAME_TEMPLATE")
    private String txtFileNameTemplate;

    @Column(name = "MIN_SEQ")
    private Integer minSeq;

    @Column(name = "MAX_SEQ")
    private Integer maxSeq;

    @Column(name = "INPUT_DIR")
    private String inputDir;

    @Column(name = "UNRATED_DIR")
    private String unratedDir;

    @Column(name = "RATED_DIR")
    private String ratedDir;

    @Column(name = "BACKUP_DIR")
    private String backupDir;

    @Column(name = "STOP_WHEN_ERROR")
    private Integer stopWhenError;

    @Column(name = "WARNING_TYPE")
    private Integer warningType;

    @Column(name = "DELAY_TIME")
    private Integer delayTime;

    @Column(name = "LOG_FILE_PATH")
    private String logFilePath;

    @Column(name = "STANDARDIZE_CLASS_NAME")
    private String standardizeClassName;

    @Column(name = "STANDARDIZE_PARAM_PATH")
    private String standardizeParamPath;

    @Column(name = "RATED_FILE_NAME_TEMPLATE")
    private String ratedFileNameTemplate;

    @Column(name = "TIME_TYPE")
    private Integer timeType;

    @Column(name = "TIME_DELAY")
    private Integer timeDelay;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "IS_BACKUP")
    private Integer isBackup;

    @Column(name = "IS_CRIPT")
    private Integer isCript;

    @Column(name = "SWITCH_ID")
    private Long switchId;

    @Column(name = "SCRIPT_BODY")
    private String scriptBody;

    @Column(name = "IS_LOAD_FROM_DB")
    private Integer isLoadFromDb;

    @Column(name = "LOAD_TABLE_NAME")
    private String loadTableName;

    @Column(name = "IMPORT_DOMAIN_CODE")
    private String importDomainCode;

    public Long getStandardizeId() {
        return standardizeId;
    }

    public void setStandardizeId(Long standardizeId) {
        this.standardizeId = standardizeId;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTxtFileNameTemplate() {
        return txtFileNameTemplate;
    }

    public void setTxtFileNameTemplate(String txtFileNameTemplate) {
        this.txtFileNameTemplate = txtFileNameTemplate;
    }

    public Integer getMinSeq() {
        return minSeq;
    }

    public void setMinSeq(Integer minSeq) {
        this.minSeq = minSeq;
    }

    public Integer getMaxSeq() {
        return maxSeq;
    }

    public void setMaxSeq(Integer maxSeq) {
        this.maxSeq = maxSeq;
    }

    public String getInputDir() {
        return inputDir;
    }

    public void setInputDir(String inputDir) {
        this.inputDir = inputDir;
    }

    public String getUnratedDir() {
        return unratedDir;
    }

    public void setUnratedDir(String unratedDir) {
        this.unratedDir = unratedDir;
    }

    public String getRatedDir() {
        return ratedDir;
    }

    public void setRatedDir(String ratedDir) {
        this.ratedDir = ratedDir;
    }

    public String getBackupDir() {
        return backupDir;
    }

    public void setBackupDir(String backupDir) {
        this.backupDir = backupDir;
    }

    public Integer getStopWhenError() {
        return stopWhenError;
    }

    public void setStopWhenError(Integer stopWhenError) {
        this.stopWhenError = stopWhenError;
    }

    public Integer getWarningType() {
        return warningType;
    }

    public void setWarningType(Integer warningType) {
        this.warningType = warningType;
    }

    public Integer getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(Integer delayTime) {
        this.delayTime = delayTime;
    }

    public String getLogFilePath() {
        return logFilePath;
    }

    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    public String getStandardizeClassName() {
        return standardizeClassName;
    }

    public void setStandardizeClassName(String standardizeClassName) {
        this.standardizeClassName = standardizeClassName;
    }

    public String getStandardizeParamPath() {
        return standardizeParamPath;
    }

    public void setStandardizeParamPath(String standardizeParamPath) {
        this.standardizeParamPath = standardizeParamPath;
    }

    public String getRatedFileNameTemplate() {
        return ratedFileNameTemplate;
    }

    public void setRatedFileNameTemplate(String ratedFileNameTemplate) {
        this.ratedFileNameTemplate = ratedFileNameTemplate;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Integer getTimeDelay() {
        return timeDelay;
    }

    public void setTimeDelay(Integer timeDelay) {
        this.timeDelay = timeDelay;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getIsBackup() {
        return isBackup;
    }

    public void setIsBackup(Integer isBackup) {
        this.isBackup = isBackup;
    }

    public Integer getIsCript() {
        return isCript;
    }

    public void setIsCript(Integer isCript) {
        this.isCript = isCript;
    }

    public Long getSwitchId() {
        return switchId;
    }

    public void setSwitchId(Long switchId) {
        this.switchId = switchId;
    }

    public String getScriptBody() {
        return scriptBody;
    }

    public void setScriptBody(String scriptBody) {
        this.scriptBody = scriptBody;
    }

    public Integer getIsLoadFromDb() {
        return isLoadFromDb;
    }

    public void setIsLoadFromDb(Integer isLoadFromDb) {
        this.isLoadFromDb = isLoadFromDb;
    }

    public String getLoadTableName() {
        return loadTableName;
    }

    public void setLoadTableName(String loadTableName) {
        this.loadTableName = loadTableName;
    }

    public String getImportDomainCode() {
        return importDomainCode;
    }

    public void setImportDomainCode(String importDomainCode) {
        this.importDomainCode = importDomainCode;
    }
}
