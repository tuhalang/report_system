package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "PRE_INTER_NUM_GROUP")
public class PreInterNumGroup {

    @Id
    @Column(name = "PRE_INTER_NUM_GROUP_ID")
    //@GeneratedValue(generator = "PRE_INTER_NUM_GROUP_SEQ")
    //@SequenceGenerator(schema = "HTDS", name = "PRE_INTER_NUM_GROUP_SEQ", sequenceName = "INTER_CODE_PRICE_SEQ", allocationSize = 1)
    private BigDecimal preInterNumGroupId;

    @Column(name = "NUMBER_GROUP_NAME")
    private String numberGroupName;

    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "INTER_CODE_PRICE_ID")
    private Long interCodePriceId;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "DATE_APPLY")
    private Date dateApply;

    public BigDecimal getPreInterNumGroupId() {
        return preInterNumGroupId;
    }

    public void setPreInterNumGroupId(BigDecimal preInterNumGroupId) {
        this.preInterNumGroupId = preInterNumGroupId;
    }

    public String getNumberGroupName() {
        return numberGroupName;
    }

    public void setNumberGroupName(String numberGroupName) {
        this.numberGroupName = numberGroupName;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getInterCodePriceId() {
        return interCodePriceId;
    }

    public void setInterCodePriceId(Long interCodePriceId) {
        this.interCodePriceId = interCodePriceId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getDateApply() {
        return dateApply;
    }

    public void setDateApply(Date dateApply) {
        this.dateApply = dateApply;
    }
}
