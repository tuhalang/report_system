package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VOICE_BLOCK")
public class VoiceBlock {

    @Id
    @Column(name = "VOICE_BLOCK_ID")
    private Long voiceBlockId;

    @Column(name = "BLOCK_CODE")
    private String blockCode;

    @Column(name = "BLOCK_NAME")
    private String blockName;

    @Column(name = "BLOCK_SORT_COUNT")
    private Integer blockSortCount;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    public Long getVoiceBlockId() {
        return voiceBlockId;
    }

    public void setVoiceBlockId(Long voiceBlockId) {
        this.voiceBlockId = voiceBlockId;
    }

    public String getBlockCode() {
        return blockCode;
    }

    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public Integer getBlockSortCount() {
        return blockSortCount;
    }

    public void setBlockSortCount(Integer blockSortCount) {
        this.blockSortCount = blockSortCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}
