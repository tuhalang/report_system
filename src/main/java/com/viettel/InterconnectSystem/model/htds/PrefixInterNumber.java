package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "PREFIX_INTER_NUMBER")
public class PrefixInterNumber {

    @Id
    @GeneratedValue(generator = "PREFIX_INTER_NUMBER_SEQ")
    @SequenceGenerator(schema = "HTDS", name = "PREFIX_INTER_NUMBER_SEQ", sequenceName = "INTER_CODE_PRICE_SEQ", allocationSize = 1)
    @Column(name = "PREFIX_INTER_NUMBER_ID")
    private BigDecimal prefixInterNumberId;

    @Column(name = "PARTNER_ID")
    private Long partnerId;

    @Column(name = "PART_TYPE")
    private String partType;

    @Column(name = "PREFIX_NUMBER")
    private String prefixNumber;

    @Column(name = "LENGTH_NUMBER")
    private Integer lengthNumber;

    @Column(name = "COUNTRY_ID")
    private Long countryId;

    @Column(name = "NUMBER_FUNCTION")
    private Integer numberFunction;

    @Column(name = "ADD_PREFIX")
    private String addPrefix;

    @Column(name = "PRE_INTER_NUM_GROUP_ID")
    private BigDecimal preInterNumGroupId;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "DATE_APPLY")
    private Date dateApply;

    @Column(name = "DATE_END")
    private Date dateEnd;

    @Column(name = "TIME_OFF_PEAK_ID")
    private Long timeOffPeakId;

    @Column(name = "PRICE_0")
    private Double price0;

    @Column(name = "PRICE_1")
    private Double price1;

    @Column(name = "PRICE_2")
    private Double price2;

    @Column(name = "PRICE_3")
    private Double price3;

    @Column(name = "PRICE_4")
    private Double price4;

    @Column(name = "CURRENCY_ID")
    private Long currencyId;

    @Column(name = "AREA_ID")
    private Long areaId;

    public Date getDateApply() {
        return dateApply;
    }

    public void setDateApply(Date dateApply) {
        this.dateApply = dateApply;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public BigDecimal getPrefixInterNumberId() {
        return prefixInterNumberId;
    }

    public void setPrefixInterNumberId(BigDecimal prefixInterNumberId) {
        this.prefixInterNumberId = prefixInterNumberId;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartType() {
        return partType;
    }

    public void setPartType(String partType) {
        this.partType = partType;
    }

    public String getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(String prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public Integer getLengthNumber() {
        return lengthNumber;
    }

    public void setLengthNumber(Integer lengthNumber) {
        this.lengthNumber = lengthNumber;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Integer getNumberFunction() {
        return numberFunction;
    }

    public void setNumberFunction(Integer numberFunction) {
        this.numberFunction = numberFunction;
    }

    public String getAddPrefix() {
        return addPrefix;
    }

    public void setAddPrefix(String addPrefix) {
        this.addPrefix = addPrefix;
    }

    public BigDecimal getPreInterNumGroupId() {
        return preInterNumGroupId;
    }

    public void setPreInterNumGroupId(BigDecimal preInterNumGroupId) {
        this.preInterNumGroupId = preInterNumGroupId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Long getTimeOffPeakId() {
        return timeOffPeakId;
    }

    public void setTimeOffPeakId(Long timeOffPeakId) {
        this.timeOffPeakId = timeOffPeakId;
    }

    public Double getPrice0() {
        return price0;
    }

    public void setPrice0(Double price0) {
        this.price0 = price0;
    }

    public Double getPrice1() {
        return price1;
    }

    public void setPrice1(Double price1) {
        this.price1 = price1;
    }

    public Double getPrice2() {
        return price2;
    }

    public void setPrice2(Double price2) {
        this.price2 = price2;
    }

    public Double getPrice3() {
        return price3;
    }

    public void setPrice3(Double price3) {
        this.price3 = price3;
    }

    public Double getPrice4() {
        return price4;
    }

    public void setPrice4(Double price4) {
        this.price4 = price4;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
}
