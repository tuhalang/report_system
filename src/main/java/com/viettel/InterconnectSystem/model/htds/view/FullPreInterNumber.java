package com.viettel.InterconnectSystem.model.htds.view;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "FULL_PRE_INTER_NUMBER")
public class FullPreInterNumber {

    @Id
    @Column(name = "ROW_ID")
    private Long rowId;

//lấy ra các trường này export ra excel:
// number_group_name, prefix_number, price_0, date_apply
// => Desctination, Numbering plan, Rates per minute, Rate valid from

    @Column(name = "PREFIX_NUMBER")
    private String prefixNumber;

    @Column(name = "NUMBER_GROUP_NAME")
    private String numberGroupName;

    @Column(name = "PRICE_0")
    private Double price;

    @Column(name = "DATE_APPLY")
    private Date dateApply;

    @Column(name = "PARTNER_ID")
    private Long partnerId;
    
    @Column(name = "INTER_TYPE")
    private String interType;

    @Column(name = "DIRECTION")
    private String direction;


    public Long getRowId() {
        return rowId;
    }

    public void setRowId(Long rowId) {
        this.rowId = rowId;
    }

    public String getPrefixNumber() {
        return prefixNumber;
    }

    public void setPrefixNumber(String prefixNumber) {
        this.prefixNumber = prefixNumber;
    }

    public String getNumberGroupName() {
        return numberGroupName;
    }

    public void setNumberGroupName(String numberGroupName) {
        this.numberGroupName = numberGroupName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDateApply() {
        return dateApply;
    }

    public void setDateApply(Date dateApply) {
        this.dateApply = dateApply;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getInterType() {
        return interType;
    }

    public void setInterType(String interType) {
        this.interType = interType;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
