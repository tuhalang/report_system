package com.viettel.InterconnectSystem.model.htds;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "GET_PROCESS_PARAM")
public class GetProcessParam {

    @Id
    @GeneratedValue(generator = "GET_PROCESS_PARAM_SEQ")
    @SequenceGenerator(schema = "HTDS",
            name = "GET_PROCESS_PARAM_SEQ",
            sequenceName = "GET_PROCESS_PARAM_SEQ",
            allocationSize = 1)
    @Column(name = "GET_ID")
    private Long getId;

    @Column(name = "DOMAIN_CODE")
    private String domainCode;

    @Column(name = "PROCESS_CODE")
    private String processCode;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "FTP_HOST")
    private String ftpHost;

    @Column(name = "FTP_USERNAME")
    private String ftpUsername;

    @Column(name = "FTP_PORT")
    private Integer ftpPort;

    @Column(name = "FTP_PASSWORD")
    private String ftpPassword;

    @Column(name = "FTP_MODE")
    private Integer ftpMode;

    @Column(name = "FTP_TRANSFER_TYPE")
    private Integer ftpTransferType;

    @Column(name = "REMOTE_DIR")
    private String remoteDir;

    @Column(name = "TEMP_DIR")
    private String tempDir;

    @Column(name = "LOCAL_DIR")
    private String localDir;

    @Column(name = "REMOTE_FILENAME_TEMPLATE")
    private String remoteFilenameTemplate;

    @Column(name = "LOCAL_FILENAME_TEMPLATE")
    private String localFilenameTemplate;

    @Column(name = "MIN_SEQ")
    private Integer minSeq;

    @Column(name = "MAX_SEQ")
    private Integer maxSeq;

    @Column(name = "SEQ_STEP")
    private Integer seqStep;

    @Column(name = "CUR_SEQ")
    private Long curSeq;

    @Column(name = "STOP_WHEN_ERROR")
    private Integer stopWhenError;

    @Column(name = "WARNING_TYPE")
    private Integer warningType;

    @Column(name = "DELAY_TIME")
    private Integer delayTime;

    @Column(name = "LOG_FILE_PATH")
    private String logFilePath;

    @Column(name = "CUR_TIMESTAMP")
    private Date curTimestamp;

    @Column(name = "DELETE_AFTER_GET")
    private Integer deleteAfterGet;

    @Column(name = "BACKUP_BEFORE_DELETE")
    private Integer backupBeforeDelete;

    @Column(name = "BACKUP_DIR")
    private String backupDir;

    @Column(name = "TIME_TYPE")
    private Integer timeType;

    @Column(name = "TIME_DELAY")
    private Integer timeDelay;

    @Column(name = "SWITCH_ID")
    private Long switchId;

    @Column(name = "IS_DELETE")
    private Integer isDelete;

    @Column(name = "STOP_WHEN_STEP_ERR")
    private Integer stopWhenStepErr;

    @Column(name = "PRIORITY_TYPE")
    private Integer priorityType;

    @Column(name = "GET_CLASS")
    private String getClass;

    public Long getGetId() {
        return getId;
    }

    public void setGetId(Long getId) {
        this.getId = getId;
    }

    public String getDomainCode() {
        return domainCode;
    }

    public void setDomainCode(String domainCode) {
        this.domainCode = domainCode;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFtpHost() {
        return ftpHost;
    }

    public void setFtpHost(String ftpHost) {
        this.ftpHost = ftpHost;
    }

    public String getFtpUsername() {
        return ftpUsername;
    }

    public void setFtpUsername(String ftpUsername) {
        this.ftpUsername = ftpUsername;
    }

    public Integer getFtpPort() {
        return ftpPort;
    }

    public void setFtpPort(Integer ftpPort) {
        this.ftpPort = ftpPort;
    }

    public String getFtpPassword() {
        return ftpPassword;
    }

    public void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    public Integer getFtpMode() {
        return ftpMode;
    }

    public void setFtpMode(Integer ftpMode) {
        this.ftpMode = ftpMode;
    }

    public Integer getFtpTransferType() {
        return ftpTransferType;
    }

    public void setFtpTransferType(Integer ftpTransferType) {
        this.ftpTransferType = ftpTransferType;
    }

    public String getRemoteDir() {
        return remoteDir;
    }

    public void setRemoteDir(String remoteDir) {
        this.remoteDir = remoteDir;
    }

    public String getTempDir() {
        return tempDir;
    }

    public void setTempDir(String tempDir) {
        this.tempDir = tempDir;
    }

    public String getLocalDir() {
        return localDir;
    }

    public void setLocalDir(String localDir) {
        this.localDir = localDir;
    }

    public String getRemoteFilenameTemplate() {
        return remoteFilenameTemplate;
    }

    public void setRemoteFilenameTemplate(String remoteFilenameTemplate) {
        this.remoteFilenameTemplate = remoteFilenameTemplate;
    }

    public String getLocalFilenameTemplate() {
        return localFilenameTemplate;
    }

    public void setLocalFilenameTemplate(String localFilenameTemplate) {
        this.localFilenameTemplate = localFilenameTemplate;
    }

    public Integer getMinSeq() {
        return minSeq;
    }

    public void setMinSeq(Integer minSeq) {
        this.minSeq = minSeq;
    }

    public Integer getMaxSeq() {
        return maxSeq;
    }

    public void setMaxSeq(Integer maxSeq) {
        this.maxSeq = maxSeq;
    }

    public Integer getSeqStep() {
        return seqStep;
    }

    public void setSeqStep(Integer seqStep) {
        this.seqStep = seqStep;
    }

    public Long getCurSeq() {
        return curSeq;
    }

    public void setCurSeq(Long curSeq) {
        this.curSeq = curSeq;
    }

    public Integer getStopWhenError() {
        return stopWhenError;
    }

    public void setStopWhenError(Integer stopWhenError) {
        this.stopWhenError = stopWhenError;
    }

    public Integer getWarningType() {
        return warningType;
    }

    public void setWarningType(Integer warningType) {
        this.warningType = warningType;
    }

    public Integer getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(Integer delayTime) {
        this.delayTime = delayTime;
    }

    public String getLogFilePath() {
        return logFilePath;
    }

    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    public Date getCurTimestamp() {
        return curTimestamp;
    }

    public void setCurTimestamp(Date curTimestamp) {
        this.curTimestamp = curTimestamp;
    }

    public Integer getDeleteAfterGet() {
        return deleteAfterGet;
    }

    public void setDeleteAfterGet(Integer deleteAfterGet) {
        this.deleteAfterGet = deleteAfterGet;
    }

    public Integer getBackupBeforeDelete() {
        return backupBeforeDelete;
    }

    public void setBackupBeforeDelete(Integer backupBeforeDelete) {
        this.backupBeforeDelete = backupBeforeDelete;
    }

    public String getBackupDir() {
        return backupDir;
    }

    public void setBackupDir(String backupDir) {
        this.backupDir = backupDir;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Integer getTimeDelay() {
        return timeDelay;
    }

    public void setTimeDelay(Integer timeDelay) {
        this.timeDelay = timeDelay;
    }

    public Long getSwitchId() {
        return switchId;
    }

    public void setSwitchId(Long switchId) {
        this.switchId = switchId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Integer getStopWhenStepErr() {
        return stopWhenStepErr;
    }

    public void setStopWhenStepErr(Integer stopWhenStepErr) {
        this.stopWhenStepErr = stopWhenStepErr;
    }

    public Integer getPriorityType() {
        return priorityType;
    }

    public void setPriorityType(Integer priorityType) {
        this.priorityType = priorityType;
    }

    public String getGetClass() {
        return getClass;
    }

    public void setGetClass(String getClass) {
        this.getClass = getClass;
    }
}
