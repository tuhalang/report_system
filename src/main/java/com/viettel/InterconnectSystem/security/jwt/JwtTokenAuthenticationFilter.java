package com.viettel.InterconnectSystem.security.jwt;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {

    private JwtTokenProvider jwtTokenProvider;

    private static final String ROLE_STAFF = "STAFF";
    private static final String ROLE_REPORTER = "REPORTER";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final String STAFF = "components";
    private static final String REPORTER = "reports";
    private static final String IMPORTS = "import";
    private static final String PARTNER = "partners";

    public JwtTokenAuthenticationFilter(JwtTokenProvider jwtTokenProvider){
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = jwtTokenProvider.resolveToken(request);
        if (token != null && jwtTokenProvider.validateToken(token)) {
            //Authentication auth = jwtTokenProvider.getAuthentication(token);

            //if (auth != null) {
                String role = jwtTokenProvider.getRole(token);

                if(request.getRequestURL().indexOf(IMPORTS) != -1){
                    if(!role.equalsIgnoreCase(ROLE_ADMIN) && !role.equalsIgnoreCase(ROLE_REPORTER)){
                        return;
                    }
                }
                if(request.getRequestURL().indexOf(STAFF) != -1){
                    if(!role.equalsIgnoreCase(ROLE_STAFF) && !role.equalsIgnoreCase(ROLE_ADMIN)){
                        if(request.getRequestURL().indexOf(PARTNER) == -1){
                            return;
                        }
                    }
                }
                if(request.getRequestURL().indexOf(REPORTER) != -1){
                    if(!role.equalsIgnoreCase(ROLE_REPORTER) && !role.equalsIgnoreCase(ROLE_ADMIN)){
                        return;
                    }
                }
                //SecurityContextHolder.getContext().setAuthentication(auth);
            //}
        }
        filterChain.doFilter(request, response);
    }
}
