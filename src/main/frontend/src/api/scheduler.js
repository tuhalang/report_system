import request from '@/utils/request'

export function fetctScheduler(query) {
    return request({
      url: '/api/v1/reports/schedulers',
      method: 'get',
      params: query
    })
  }

  export function deleteScheduler(synthTypeId, deleted){
    return request({
      url:'/api/v1/reports/schedulers',
      method: 'delete',
      params: {
        synthTypeId: synthTypeId,
        deleted: deleted
      }
    })
  }

  export function createScheduler(data) {
    return request({
      url: '/api/v1/reports/schedulers',
      method: 'post',
      data
    })
  }

  export function updateScheduler(data) {
    return request({
      url: '/api/v1/reports/schedulers',
      method: 'put',
      data
    })
  }
