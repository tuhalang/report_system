import request from '@/utils/request'

export function fetctAreas(query) {
    return request({
      url: '/api/v1/components/areas',
      method: 'get',
      params: query
    })
  }