import request from '@/utils/request'

export function fetctVoiceBlocks(query) {
    return request({
      url: '/api/v1/components/voice-blocks',
      method: 'get',
      params: query
    })
  }