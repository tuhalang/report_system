import request from '@/utils/request'

export function fetctInterPrice(query) {
  return request({
    url: '/api/v1/reports/inter-price',
    method: 'get',
    responseType: 'arraybuffer',
    params: query
  })
}
