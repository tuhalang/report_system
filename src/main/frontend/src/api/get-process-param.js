import request from '@/utils/request'

export function fetctGetParams(query) {
    return request({
      url: '/api/v1/components/get-process-params/',
      method: 'get',
      params: query
    })
  }

  export function deleteGetParam(getId, deleted){
    return request({
      url:'/api/v1/components/get-process-params/',
      method: 'delete',
      params: {
        getId: getId,
        deleted: deleted
      }
    })
  }

  export function createGetParam(data) {
    return request({
      url: '/api/v1/components/get-process-params',
      method: 'post',
      data
    })
  }

  export function updateGetParam(data) {
    return request({
      url: '/api/v1/components/get-process-params',
      method: 'put',
      data
    })
  }
