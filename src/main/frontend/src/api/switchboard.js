import request from '@/utils/request'

export function fetctSwitchBoards(query) {
    return request({
      url: '/api/v1/components/switchboards',
      method: 'get',
      params: query
    })
  }

  export function deleteSwitchBoard(switchBoardId, deleted){
    return request({
      url:'/api/v1/components/switchboards',
      method: 'delete',
      params: {
        switchId: switchBoardId,
        deleted: deleted
      }
    })
  }

  export function createSwitchBoard(data) {
    return request({
      url: '/api/v1/components/switchboards',
      method: 'post',
      data
    })
  }

  export function updateSwitchBoard(data) {
    return request({
      url: '/api/v1/components/switchboards',
      method: 'put',
      data
    })
  }
