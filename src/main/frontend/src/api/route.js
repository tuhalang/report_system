import request from '@/utils/request'

export function fetctRoutes(query) {
    return request({
      url: '/api/v1/components/routes',
      method: 'get',
      params: query
    })
  }

  export function deleteRoute(routeId, deleted){
    return request({
      url:'/api/v1/components/routes',
      method: 'delete',
      params: {
        routeId: routeId,
        deleted: deleted
      }
    })
  }

  export function createRoute(data) {
    return request({
      url: '/api/v1/components/routes',
      method: 'post',
      data
    })
  }

  export function updateRoute(data) {
    return request({
      url: '/api/v1/components/routes',
      method: 'put',
      data
    })
  }
