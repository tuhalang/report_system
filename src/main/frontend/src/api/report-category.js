import request from '@/utils/request'

export function fetchReportCategories(query) {
    return request({
      url: '/api/v1/reports/report-categories',
      method: 'get',
      params: query
    })
  }

  export function deleteReportCategory(reportId, deleted){
    return request({
      url:'/api/v1/reports/report-categories',
      method: 'delete',
      params: {
        reportId: reportId,
        deleted: deleted
      }
    })
  }

  export function createReportCategory(data) {
    return request({
      url: '/api/v1/reports/report-categories',
      method: 'post',
      data
    })
  }

  export function updateReportCategory(data) {
    return request({
      url: '/api/v1/reports/report-categories',
      method: 'put',
      data
    })
  }
