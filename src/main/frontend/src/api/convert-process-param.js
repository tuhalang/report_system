import request from '@/utils/request'

export function fetctConvertParams(query) {
    return request({
      url: '/api/v1/components/convert-process-params/',
      method: 'get',
      params: query
    })
  }

  export function deleteConvertParam(convertId, deleted){
    return request({
      url:'/api/v1/components/convert-process-params/',
      method: 'delete',
      params: {
        convertId: convertId,
        deleted: deleted
      }
    })
  }

  export function createConvertParam(data) {
    return request({
      url: '/api/v1/components/convert-process-params',
      method: 'post',
      data
    })
  }

  export function updateConvertParam(data) {
    return request({
      url: '/api/v1/components/convert-process-params',
      method: 'put',
      data
    })
  }
