import request from '@/utils/request'

export function fetchPartners(query) {
  return request({
    url: '/api/v1/components/partners',
    method: 'get',
    params: query
  })
}

export function fetchArticle(id) {
  return request({
    url: '/vue-element-admin/article/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/article/pv',
    method: 'get',
    params: { pv }
  })
}

export function createPartner(data) {
  return request({
    url: '/api/v1/components/partners',
    method: 'post',
    data
  })
}

export function updatePartner(data) {
  return request({
    url: '/api/v1/components/partners',
    method: 'put',
    data
  })
}

export function deletePartner(partnerId, deleted){
  return request({
    url:'/api/v1/components/partners/',
    method: 'delete',
    params: {
      partnerId: partnerId,
      deleted: deleted
    }
  })
}
