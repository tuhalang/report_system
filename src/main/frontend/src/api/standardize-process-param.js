import request from '@/utils/request'

export function fetctStandardizeParams(query) {
    return request({
      url: '/api/v1/components/standardize-process-params/',
      method: 'get',
      params: query
    })
  }

  export function deleteStandardizeParam(standardizeId, deleted){
    return request({
      url:'/api/v1/components/standardize-process-params/',
      method: 'delete',
      params: {
        standardizeId: standardizeId,
        deleted: deleted
      }
    })
  }

  export function createStandardizeParam(data) {
    return request({
      url: '/api/v1/components/standardize-process-params',
      method: 'post',
      data
    })
  }

  export function updateStandardizeParam(data) {
    return request({
      url: '/api/v1/components/standardize-process-params',
      method: 'put',
      data
    })
  }
