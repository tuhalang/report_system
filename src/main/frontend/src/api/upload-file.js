import request from '@/utils/request'

export function uploadFile(data) {
    return request({
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      url: '/api/v1/import/prices',
      method: 'post',
      data
    })
  }

export function getTemplate(){
  return request({
    url: '/api/v1/import/template',
    method: 'get',
    responseType: 'arraybuffer',
  })
}