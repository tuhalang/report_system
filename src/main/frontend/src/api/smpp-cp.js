import request from '@/utils/request'

export function fetctSmppCp(query) {
    return request({
      url: '/api/v1/components/smppcps',
      method: 'get',
      params: query
    })
  }

  export function deleteSmppCp(smppCpId, deleted){
    return request({
      url:'/api/v1/components/smppcps',
      method: 'delete',
      params: {
        smppCpId: smppCpId,
        deleted: deleted
      }
    })
  }

  export function createSmppCp(data) {
    return request({
      url: '/api/v1/components/smppcps',
      method: 'post',
      data
    })
  }

  export function updateSmppCp(data) {
    return request({
      url: '/api/v1/components/smppcps',
      method: 'put',
      data
    })
  }
