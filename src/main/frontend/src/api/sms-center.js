import request from '@/utils/request'

export function fetctSmsCenter(query) {
    return request({
      url: '/api/v1/components/smscenters',
      method: 'get',
      params: query
    })
  }

  export function deleteSmsCenter(smsCenterId, deleted){
    return request({
      url:'/api/v1/components/smscenters',
      method: 'delete',
      params: {
        smsCenterId: smsCenterId,
        deleted: deleted
      }
    })
  }

  export function createSmsCenter(data) {
    return request({
      url: '/api/v1/components/smscenters',
      method: 'post',
      data
    })
  }

  export function updateSmsCenter(data) {
    return request({
      url: '/api/v1/components/smscenters',
      method: 'put',
      data
    })
  }
