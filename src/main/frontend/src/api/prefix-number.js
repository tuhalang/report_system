import request from '@/utils/request'

export function fetchPrefixNumbers(query) {
    return request({
      url: '/api/v1/components/prefix-numbers',
      method: 'get',
      params: query
    })
  }

  export function deletePrefixNumber(prefixNumberId, deleted){
    return request({
      url:'/api/v1/components/prefix-numbers',
      method: 'delete',
      params: {
        prefixNumberId: prefixNumberId,
        deleted: deleted
      }
    })
  }

  export function createPrefixNumber(data) {
    return request({
      url: '/api/v1/components/prefix-numbers',
      method: 'post',
      data
    })
  }

  export function updatePrefixNumber(data) {
    return request({
      url: '/api/v1/components/prefix-numbers',
      method: 'put',
      data
    })
  }
