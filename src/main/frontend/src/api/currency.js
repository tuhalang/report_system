import request from '@/utils/request'

export function fetctCurrencies() {
    return request({
      url: '/api/v1/components/currencies',
      method: 'get',
    })
  }