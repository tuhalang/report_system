import request from '@/utils/request'

export function fetctServices(query) {
    return request({
      url: '/api/v1/components/services',
      method: 'get',
      params: query
    })
  }

  export function deleteService(serviceId, deleted){
    return request({
      url:'/api/v1/components/services',
      method: 'delete',
      params: {
        serviceId: serviceId,
        deleted: deleted
      }
    })
  }

  export function createService(data) {
    return request({
      url: '/api/v1/components/services',
      method: 'post',
      data
    })
  }

  export function updateService(data) {
    return request({
      url: '/api/v1/components/services',
      method: 'put',
      data
    })
  }
