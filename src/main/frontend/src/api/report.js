import request from '@/utils/request'

export function fetctReport(query) {
    return request({
      url: '/api/v1/reports/report',
      method: 'get',
      responseType: 'arraybuffer',
      params: query
    })
  }