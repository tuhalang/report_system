<template>
  <div class="app-container">
    <div class="filter-container">
      <el-input v-model="listQuery.key" placeholder="Name" style="width: 500px;padding-right:20px;" class="filter-item" @keyup.enter.native="handleFilter" />
      <el-button v-waves class="filter-item" type="primary" icon="fas fa-search" @click="handleFilter">
        Search
      </el-button>
      <el-button class="filter-item" style="margin-left: 10px;" type="primary" icon="fas fa-edit" @click="handleCreate">
        Add
      </el-button>
      <el-button v-waves :loading="downloadLoading" class="filter-item" type="primary" icon="fas fa-download" @click="handleDownload">
        Export
      </el-button>
      <el-checkbox v-model="showDelete" class="filter-item" style="margin-left:15px;" @change="viewDelete()">
        View deleted
      </el-checkbox>
    </div>

    <el-table
      v-loading="listLoading"
      :data="list"
      border
      fit
      highlight-current-row
      style="width: 100%;"
    >
      <el-table-column label="ID" align="center" width="60">
        <template slot-scope="{row}">
          <span>{{ row.route.routeId }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Route Name" align="center" width="105">
        <template slot-scope="{row}">
          <span>{{ row.route.routeName }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Switch Name" align="center" width="110">
        <template slot-scope="{row}">
          <span>{{ row.switchName }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Prefix Number" align="center" width="120">
        <template slot-scope="{row}">
          <span>{{ row.prefixNumber }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Partner Name" align="center" width="120">
        <template slot-scope="{row}">
          <span>{{ row.partnerName }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Part Type" align="center" width="90">
        <template slot-scope="{row}">
          <span>{{ row.route.partType }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Connect Switch Type" align="center" width="130">
        <template slot-scope="{row}">
          <span>{{ row.route.connectSwitchType }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Connect Area Id" align="center" width="85">
        <template slot-scope="{row}">
          <span>{{ row.route.connectAreaId }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Service Name" align="center" width="120">
        <template slot-scope="{row}">
          <span>{{ row.serviceName }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Direction Name" align="center" width="90">
        <template slot-scope="{row}">
          <span>{{ row.route.directionName }}</span>
        </template>
      </el-table-column>

      <el-table-column label="Update Time" align="center" width="110">
        <template slot-scope="{row}">
          <span>{{ formatDate(row.route.updateTime) }}</span>
        </template>
      </el-table-column>

      <!-- <el-table-column  label="Isdn"  align="center" width="100" >
        <template slot-scope="{row}">
          <span>{{ row.route.isdn }}</span>
        </template>
      </el-table-column>

      <el-table-column  label="Khtk Name"  align="center" width="100" >
        <template slot-scope="{row}">
          <span>{{ row.route.khtkName }}</span>
        </template>
      </el-table-column> -->

      <el-table-column label="Is Delete" align="center" width="80">
        <template slot-scope="{row}">
          <span v-if="row.route.isDelete">True</span>
          <span v-if="!row.route.isDelete">False</span>
        </template>
      </el-table-column>

      <el-table-column label="Actions" align="center" width="230" class-name="small-padding fixed-width">
        <template slot-scope="{row,$index}">
          <el-button v-if="!row.route.isDelete" type="primary" size="mini" @click="handleUpdate(row)">
            Edit
          </el-button>
          <el-popconfirm
            v-if="!row.route.isDelete"
            confirm-button-text="OK"
            cancel-button-text="No, Thanks"
            icon="fas fa-info-circle"
            icon-color="red"
            title="Are you sure to delete this?"
            @onConfirm="handleDelete(row,$index,1)"
          >
            <el-button slot="reference" size="mini" type="danger">
              Delete
            </el-button>
          </el-popconfirm>

          <el-popconfirm
            v-if="row.route.isDelete"
            confirm-button-text="OK"
            cancel-button-text="No, Thanks"
            icon="fas fa-info-circle"
            icon-color="red"
            title="Are you sure to restore this?"
            @onConfirm="handleDelete(row,$index,0)"
          >
            <el-button slot="reference" size="mini" type="danger">
              Restore
            </el-button>
          </el-popconfirm>
        </template>
      </el-table-column>
    </el-table>

    <pagination v-show="total>0" :total="total" :page.sync="listQuery.page" :limit.sync="listQuery.limit" @pagination="getList" />

    <el-dialog :title="textMap[dialogStatus]" :visible.sync="dialogFormVisible" center width="90%">
      <el-form ref="dataForm" :rules="rules" :model="temp" label-position="left" label-width="170px" style="width: 90%; margin-left:50px;">
        <el-row :gutter="40">
          <el-col :span="12">
            <el-form-item v-show="dialogStatus === 'update'" label="Id" prop="routeId">
              <el-input v-model="temp.routeId" :disabled="true" />
            </el-form-item>
            <el-form-item label="Route Name" prop="routeName">
              <el-input v-model="temp.routeName" />
            </el-form-item>
            <el-form-item label="Switch Name" prop="switchId">
              <el-autocomplete
                v-model="nameTemp.switchName"
                clearable
                :fetch-suggestions="querySearchAsyncSwitch"
                placeholder="Please input switch name"
                @select="handleSelectSwitch"
              />
            </el-form-item>
            <el-form-item label="Prefix Number" prop="prefixNumberId">
              <el-autocomplete
                v-model="nameTemp.prefixNumber"
                clearable
                :fetch-suggestions="querySearchAsyncPrefixNumber"
                placeholder="Please input prefix number"
                @select="handleSelectPrefixNumber"
              />
            </el-form-item>
            <el-form-item label="Part Name" prop="partnerId">
              <el-autocomplete
                v-model="nameTemp.partnerName"
                clearable
                :fetch-suggestions="querySearchAsyncPartner"
                placeholder="Please input part name"
                @select="handleSelectPartner"
              />
            </el-form-item>

          </el-col>
          <el-col :span="12">
            <el-form-item label="Part Type" prop="partType">
              <el-input v-model="temp.partType" />
            </el-form-item>
            <el-form-item label="Connect Switch Type" prop="connectSwitchType">
              <el-input v-model="temp.connectSwitchType" />
            </el-form-item>
            <el-form-item label="Connect Area Id" prop="connectAreaId">
              <el-input v-model="temp.connectAreaId" />
            </el-form-item>
            <el-form-item label="Service Name" prop="serviceId">
              <el-autocomplete
                v-model="nameTemp.serviceName"
                clearable
                :fetch-suggestions="querySearchAsyncService"
                placeholder="Please input service name"
                @select="handleSelectService"
              />
            </el-form-item>
            <el-form-item label="Direction Name" prop="directionName">
              <el-input v-model="temp.directionName" />
            </el-form-item>
            <!-- <el-form-item label="Update Time" prop="updateTime">
              <el-date-picker
                  v-model="temp.updateTime"
                  type="date"
                  placeholder="Pick a day">
                </el-date-picker>
            </el-form-item> -->
            <!-- <el-form-item label="Isdn" prop="isdn">
              <el-input v-model="temp.isdn"/>
            </el-form-item>
            <el-form-item label="KhtkName" prop="khtkName">
              <el-input v-model="temp.khtkName"/>
            </el-form-item> -->
            <el-form-item v-show="dialogStatus === 'update'" label="Is Delete" prop="isDelete">
              <el-select v-model="temp.isDelete" class="filter-item" placeholder="Please select">
                <el-option v-for="item in deleteOptions" :key="item.key" :label="item.display_name" :value="item.key" />
              </el-select>
            </el-form-item>
          </el-col>
        </el-row>
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button @click="dialogFormVisible = false">
          Cancel
        </el-button>
        <el-button type="primary" @click="dialogStatus==='create'?createData():updateData()">
          Confirm
        </el-button>
      </div>
    </el-dialog>
  </div>
</template>

<script>
import { fetctRoutes, createRoute, updateRoute, deleteRoute } from '@/api/route'
import waves from '@/directive/waves' // waves directive
import { parseTime } from '@/utils'
import Pagination from '@/components/Pagination' // secondary package based on el-pagination
import { fetctSwitchBoards } from '@/api/switchboard'
import { fetctServices } from '@/api/service'
import { fetchPartners } from '@/api/partner'
import { fetchPrefixNumbers } from '@/api/prefix-number'

const deleteOptions = [
  { key: 1, display_name: 'True' },
  { key: 0, display_name: 'False' }
]

export default {
  name: 'Country',
  components: { Pagination },
  directives: { waves },
  data() {
    return {
      list: null,
      total: 0,
      listLoading: true,
      listQuery: {
        deleted: 0,
        page: 1,
        limit: 10,
        key: null
      },
      deleteOptions,
      showDelete: false,
      temp: {
        routeId: undefined,
        routeName: undefined,
        switchId: undefined,
        prefixNumberId: undefined,
        partnerId: undefined,
        partType: undefined,
        connectSwitchType: undefined,
        connectAreaId: undefined,
        isDelete: undefined,
        serviceId: undefined,
        directionName: undefined,
        updateTime: undefined,
        isdn: undefined,
        khtkName: undefined
      },
      nameTemp: {
        partnerName: undefined,
        switchName: undefined,
        prefixNumber: undefined,
        serviceName: undefined
      },
      dialogFormVisible: false,
      dialogStatus: '',
      textMap: {
        update: 'Edit',
        create: 'Create'
      },
      downloadLoading: false,
      rules: {

        routeName: [
          { required: true, message: 'RouteName is required', trigger: 'blur' }
        ],

        switchId: [
          { required: true, message: 'SwitchId is required', trigger: 'change' }
        ],

        // prefixNumberId: [
        //   { required: true, message: 'PrefixNumberId is required', trigger: 'change' }
        // ],

        partnerId: [
          { required: true, message: 'PartnerId is required', trigger: 'change' }
        ],

        partType: [
          { required: true, message: 'PartType is required', trigger: 'blur' }
        ],

        connectSwitchType: [
          { required: true, message: 'ConnectSwitchType is required', trigger: 'blur' }
        ],

        // connectAreaId: [
        //   { required: true, message: 'ConnectAreaId is required', trigger: 'blur' }
        // ],

        isDelete: [
          { required: true, message: 'IsDelete is required', trigger: 'change' }
        ],

        // serviceId: [
        //   { required: true, message: 'ServiceId is required', trigger: 'change' }
        // ],

        directionName: [
          { required: true, message: 'DirectionName is required', trigger: 'blur' }
        ],

        updateTime: [
          { required: true, message: 'UpdateTime is required', trigger: 'blur' }
        ]

        // isdn: [
        //   { required: true, message: 'Isdn is required', trigger: 'blur' }
        // ],

        // khtkName: [
        //   { required: true, message: 'KhtkName is required', trigger: 'blur' }
        // ],

      }
    }
  },
  created() {
    this.getList()
  },
  methods: {
    querySearchAsyncSwitch(queryString, cb) {
      const query = {
        page: 1,
        limit: 5,
        deleted: 0,
        key: queryString
      }
      let results = []
      let listAreas = []
      fetctSwitchBoards(query).then(response => {
        listAreas = response.data.items
        results = listAreas.map(s => {
          return {
            value: s.switchBoard.switchName,
            key: s.switchBoard.switchId
          }
        })
        clearTimeout(this.timeout)
        this.timeout = setTimeout(() => {
          cb(results)
        }, 100)
      })
    },
    handleSelectSwitch(item) {
      console.log(item.key)
      this.temp.switchId = item.key
    },
    querySearchAsyncPrefixNumber(queryString, cb) {
      const query = {
        page: 1,
        limit: 5,
        deleted: 0,
        key: queryString
      }
      let results = []
      let listAreas = []
      fetchPrefixNumbers(query).then(response => {
        listAreas = response.data.items
        results = listAreas.map(s => {
          return {
            value: s.prefixNumber.prefixNumber,
            key: s.prefixNumber.prefixNumberId
          }
        })
        clearTimeout(this.timeout)
        this.timeout = setTimeout(() => {
          cb(results)
        }, 100)
      })
    },
    handleSelectPrefixNumber(item) {
      console.log(item.key)
      this.temp.prefixNumberId = item.key
    },
    querySearchAsyncPartner(queryString, cb) {
      const query = {
        page: 1,
        limit: 5,
        deleted: 0,
        key: queryString
      }
      let results = []
      let listAreas = []
      fetchPartners(query).then(response => {
        listAreas = response.data.items
        results = listAreas.map(s => {
          return {
            value: s.partName,
            key: s.partnerId
          }
        })
        clearTimeout(this.timeout)
        this.timeout = setTimeout(() => {
          cb(results)
        }, 100)
      })
    },
    handleSelectPartner(item) {
      console.log(item.key)
      this.temp.partnerId = item.key
    },
    querySearchAsyncService(queryString, cb) {
      const query = {
        page: 1,
        limit: 5,
        deleted: 0,
        key: queryString
      }
      let results = []
      let listAreas = []
      fetctServices(query).then(response => {
        listAreas = response.data.items
        results = listAreas.map(s => {
          return {
            value: s.service.serviceName,
            key: s.service.serviceId
          }
        })
        clearTimeout(this.timeout)
        this.timeout = setTimeout(() => {
          cb(results)
        }, 100)
      })
    },
    handleSelectService(item) {
      console.log(item.key)
      this.temp.serviceId = item.key
    },
    formatDate(d) {
      d = new Date(d)
      const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d)
      const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d)
      const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d)
      return ye + '-' + mo + '-' + da
    },
    getList() {
      this.listLoading = true
      fetctRoutes(this.listQuery).then(response => {
        this.list = response.data.items
        this.total = response.data.total
        // Just to simulate the time of the request
        setTimeout(() => {
          this.listLoading = false
        }, 0 * 1000)
      })
    },
    resetTemp() {
      this.temp = {
        routeId: undefined,
        routeName: undefined,
        switchId: undefined,
        prefixNumberId: undefined,
        partnerId: undefined,
        partType: undefined,
        connectSwitchType: undefined,
        connectAreaId: undefined,
        isDelete: undefined,
        serviceId: undefined,
        directionName: undefined,
        updateTime: undefined,
        isdn: undefined,
        khtkName: undefined
      }
      this.nameTemp = {
        partnerName: undefined,
        switchName: undefined,
        prefixNumber: undefined,
        serviceName: undefined
      }
    },
    handleCreate() {
      this.resetTemp()
      this.dialogStatus = 'create'
      this.dialogFormVisible = true
      this.$nextTick(() => {
        this.$refs['dataForm'].clearValidate()
      })
    },
    createData() {
      this.temp.isDelete = 0
      this.temp.routeId = 0
      this.temp.updateTime = new Date()
      this.$refs['dataForm'].validate((valid) => {
        if (valid) {
          this.temp.delete = 0
          createRoute(this.temp).then(() => {
            const row = {
              route: this.temp,
              partnerName: this.nameTemp.partnerName,
              switchName: this.nameTemp.switchName,
              prefixNumber: this.nameTemp.prefixNumber,
              serviceName: this.nameTemp.serviceName
            }
            this.list.unshift(row)
            this.dialogFormVisible = false
            this.$notify({
              title: 'Success',
              message: 'Created Successfully',
              type: 'success',
              duration: 2000
            })
            this.getList()
          })
        } else {
          console.log('Not valid')
        }
      })
    },
    handleUpdate(row) {
      const obj = Object.assign({}, row)
      this.temp = obj.route
      this.nameTemp.partnerName = obj.partnerName
      this.nameTemp.switchName = obj.switchName
      this.nameTemp.prefixNumber = obj.prefixNumber
      this.nameTemp.serviceName = obj.serviceName
      this.dialogStatus = 'update'
      this.dialogFormVisible = true
      this.$nextTick(() => {
        this.$refs['dataForm'].clearValidate()
      })
    },
    updateData() {
      this.$refs['dataForm'].validate((valid) => {
        if (valid) {
          const tempData = Object.assign({}, this.temp)
          tempData.updateTime = new Date()
          updateRoute(tempData).then(() => {
            const index = this.list.findIndex(v => v.route.routeId === this.temp.routeId)
            const row = {
              route: this.temp,
              partnerName: this.nameTemp.partnerName,
              switchName: this.nameTemp.switchName,
              prefixNumber: this.nameTemp.prefixNumber,
              serviceName: this.nameTemp.serviceName
            }
            this.list.splice(index, 1, row)
            this.dialogFormVisible = false
            this.$notify({
              title: 'Success',
              message: 'Update Successfully',
              type: 'success',
              duration: 2000
            })
          })
        } else {
          console.log('Not valid')
        }
      })
    },
    handleDelete(row, index, deleted) {
      deleteRoute(row.route.routeId, deleted).then(() => {
        if (deleted) {
          this.$notify({
            title: 'Success',
            message: 'Delete Successfully',
            type: 'success',
            duration: 2000
          })
        } else {
          this.$notify({
            title: 'Success',
            message: 'Restore Successfully',
            type: 'success',
            duration: 2000
          })
        }

        this.list.splice(index, 1)
      })
    },
    handleDownload() {
      this.downloadLoading = true
      import('@/vendor/Export2Excel').then(excel => {
        const tHeader = ['Route Id', 'Route Name', 'Switch Id', 'Prefix Number Id', 'Partner Id', 'Part Type', 'Connect Switch Type', 'Connect Area Id', 'Is Delete', 'Service Id', 'Direction Name', 'Update Time', 'Isdn', 'Khtk Name']
        const filterVal = ['routeId', 'routeName', 'switchId', 'prefixNumberId', 'partnerId', 'partType', 'connectSwitchType', 'connectAreaId', 'isDelete', 'serviceId', 'directionName', 'updateTime', 'isdn', 'khtkName']
        const data = this.formatJson(filterVal)
        excel.export_json_to_excel({
          header: tHeader,
          data,
          filename: 'routes'
        })
        this.downloadLoading = false
      })
    },
    formatJson(filterVal) {
      return this.list.map(v => filterVal.map(j => {
        if (j === 'timestamp') {
          return parseTime(v['route'][j])
        } else {
          return v['route'][j]
        }
      }))
    },
    viewDelete: function() {
      this.listQuery.deleted = this.showDelete ? 1 : 0
      this.getList()
    },
    handleFilter() {
      this.listQuery.page = 1
      this.getList()
    }
  }
}
</script>
