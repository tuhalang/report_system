/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'
import {getRole} from '@/utils/auth'

function checkHidenComponent(){
  let role = getRole()
  if(role === 'STAFF' || role === 'ADMIN'){
    return false;
  }
  return true;
}

const componentsRouter = {
  path: '/components',
  component: Layout,
  redirect: 'noRedirect',
  name: 'Component',
  hidden: checkHidenComponent(),
  meta: {
    title: 'Category',
    icon: 'component'
  },
  children: [
    {
      path: 'country',
      component: () => import('@/views/categories/Country'),
      name: 'Country',
      meta: { title: 'Country' }
    },
    {
      path: 'service',
      component: () => import('@/views/categories/Service'),
      name: 'Service',
      meta: { title: 'Service' }
    },
    {
      path: 'partner',
      component: () => import('@/views/categories/Partner'),
      name: 'Partner',
      meta: { title: 'Partner' }
    },
    {
      path: 'switch-board',
      component: () => import('@/views/categories/SwitchBoard'),
      name: 'SwitchBoard',
      meta: { title: 'Switch Board' }
    },
    {
      path: 'route',
      component: () => import('@/views/categories/Route'),
      name: 'Route',
      meta: { title: 'Route' }
    },
    {
      path: 'cmpp-cp',
      component: () => import('@/views/categories/SMPP_CP'),
      name: 'SMPP_CP',
      meta: { title: 'SMPP_CP' }
    },
    {
      path: 'sms-center',
      component: () => import('@/views/categories/SMSCenter'),
      name: 'SMSCenter',
      meta: { title: 'SMS Center' }
    },
    {
      path: 'get-process-param',
      component: () => import('@/views/categories/GetProcessParam'),
      name: 'GetProcessParam',
      meta: { title: 'Get Param' }
    },
    {
      path: 'convert-process-param',
      component: () => import('@/views/categories/ConverProcessParam'),
      name: 'ConverProcessParam',
      meta: { title: 'Convert Param' }
    },
    {
      path: 'standardize-process-param',
      component: () => import('@/views/categories/StandardizeProcessParam'),
      name: 'StandardizeProcessParam',
      meta: { title: 'Standardize Param' }
    },
    {
      path: 'prefix-number',
      component: () => import('@/views/categories/PrefixNumber'),
      name: 'PrefixNumber',
      meta: { title: 'Prefix Number' }
    },
    // {
    //   path: 'report-catrgory',
    //   component: () => import('@/views/categories/ReportCategory'),
    //   name: 'ReportCategory',
    //   meta: { title: 'Report Category' }
    // },
    
  ]
}

export default componentsRouter
