import Vue from 'vue'
import Router from 'vue-router'
import { getRole } from '@/utils/auth'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
import componentsRouter from './modules/components'

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    hidden: true,
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        name: 'Dashboard',
        meta: { title: 'Dashboard', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    hidden: true,
    children: [
      {
        path: 'index',
        component: () => import('@/views/profile/index'),
        name: 'Profile',
        meta: { title: 'Profile', icon: 'user', noCache: true }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [

  /** when your routing map is too long, you can split it into small modules **/
  componentsRouter,

  // {
  //   path: '/imports',
  //   component: Layout,
  //   redirect: '/excel/upload-excel',
  //   name: 'Imports',
  //   hidden: checkHidendImport(),
  //   meta: {
  //     title: 'Import',
  //     icon: 'excel'
  //   },
  //   children: [
  //     // {
  //     //   path: 'export-excel',
  //     //   component: () => import('@/views/excel/export-excel'),
  //     //   name: 'ExportExcel',
  //     //   meta: { title: 'Export Excel' }
  //     // },
  //     // {
  //     //   path: 'export-selected-excel',
  //     //   component: () => import('@/views/excel/select-excel'),
  //     //   name: 'SelectExcel',
  //     //   meta: { title: 'Export Selected' }
  //     // },
  //     // {
  //     //   path: 'export-merge-header',
  //     //   component: () => import('@/views/excel/merge-header'),
  //     //   name: 'MergeHeader',
  //     //   meta: { title: 'Merge Header' }
  //     // },
  //     {
  //       path: 'upload-excel',
  //       component: () => import('@/views/excel/upload-excel'),
  //       name: 'UploadExcel',
  //       meta: { title: 'Import Prices' }
  //     }
  //   ]
  //  },
  {
    path: '/reports',
    component: Layout,
    redirect: '/reports/export-excel',
    name: 'Excel',
    hidden: checkHidenReport(),
    meta: {
      title: 'Reports',
      icon: 'excel'
    },
    children: [
      {
        path: 'report-category',
        component: () => import('@/views/categories/ReportCategory'),
        name: 'Report Category',
        meta: { title: 'Report Category' }
      },
      {
        path: 'scheduler',
        component: () => import('@/views/categories/Scheduler'),
        name: 'Scheduler',
        meta: { title: 'Scheduler' }
      },
      {
        path: 'report',
        component: () => import('@/views/excel/export-excel'),
        name: 'Report',
        meta: { title: 'Reports' }
      },
      {
        path: 'import',
        component: () => import('@/views/excel/upload-excel'),
        name: 'UploadExcel',
        meta: { title: 'Import Prices' }
      },
      {
        path: 'report-price',
        component: () => import('@/views/excel/export-inter-price'),
        name: 'Export Inter Price',
        meta: { title: 'Export Inter Price' }
      }
    ]
  },

  // {
  //   path: '/zip',
  //   component: Layout,
  //   redirect: '/zip/download',
  //   alwaysShow: true,
  //   name: 'Zip',
  //   meta: { title: 'Zip', icon: 'zip' },
  //   children: [
  //     {
  //       path: 'download',
  //       component: () => import('@/views/zip/index'),
  //       name: 'ExportZip',
  //       meta: { title: 'Export Zip' }
  //     }
  //   ]
  // },

  // {
  //   path: '/pdf',
  //   component: Layout,
  //   redirect: '/pdf/index',
  //   children: [
  //     {
  //       path: 'index',
  //       component: () => import('@/views/pdf/index'),
  //       name: 'PDF',
  //       meta: { title: 'PDF', icon: 'pdf' }
  //     }
  //   ]
  // },
  // {
  //   path: '/pdf/download',
  //   component: () => import('@/views/pdf/download'),
  //   hidden: true
  // },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

function checkHidenReport() {
  const role = getRole()
  if (role === 'REPORTER' || role === 'ADMIN') {
    return false
  }
  return true
}

function checkHidendImport() {
  const role = getRole()
  if (role === 'ADMIN') {
    return false
  }
  return true
}

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
